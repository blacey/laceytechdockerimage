import React, {useState} from 'react'

function Search() {
    const [q, setQ] = useState("")

    return (
        <div className="search-form">
            <div className="container">
                <div className="row">
                    <form method="POST">
                        <input type="text" placeholder="Search..." className="search-input" onChange={(event) => setQ(event.target.value)} />
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Search
