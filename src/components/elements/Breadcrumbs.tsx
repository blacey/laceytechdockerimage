import React from 'react'

// Use router to get the paths for the breadcrumbs
// Add in breadcrumb SEO markup (Schema)

export default function Breadcrumbs(crumbs=[]) {
    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
                <li className="breadcrumb-item"><a href="#">Home</a></li>
                <li className="breadcrumb-item"><a href="#">Library</a></li>
                <li className="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    )
}
