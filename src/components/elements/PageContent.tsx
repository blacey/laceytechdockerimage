import React from 'react'

function PageContent({ children }) {
    return (
        <main className="main">
            {children}        
        </main>
    )
}

export default PageContent
