import React, {useState} from 'react'
import Link from 'next/link'
import SocialLinks from './SocialLinks/SocialLinks'
import ContactForm from '../forms/ContactForm'

function handleSubmit(event) {
    event.preventDefault()
    console.error('** Handle Submit Fired!')
}

function SiteFooter() {
    return (
        <footer className="site-footer">
            <div className="pre-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <h4>Why Choose Us?</h4>

                            <ul className="tick-list">
                                <li>Save you money from hiring in-house specialists</li>
                                <li>Increase customers for your business</li>
                                <li>Track and measure your marketing activities</li>
                                <li>We can make technology work for you</li>
                                <li>Setup E-Commerce or Membership websites for increased revenue</li>
                                <li>We have over 15 years experience in online marketing</li>
                            </ul>
                        </div>

                        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <h4>Contact Our Team</h4>
                            <ContactForm />
                        </div>
                    </div>
                </div>
            </div>

            <div className="footer">
                <div className="container">  
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h5>Contact Us</h5>
                            <p>
                                Lacey Tech<br />
                                Briarleas Court,<br />
                                Farnborough,<br />
                                Hampshire,<br />
                                GU14 6HL<br />
                                <br/>
                                Call Us: <a href="tel:01252518233">01252 518233</a>
                            </p>

                            <SocialLinks />
                        </div>

                        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h5>Recent Projects</h5>              
                            <ul>
                                <li><a href="https://divansahab.com" target="_blank">Divan Sahab</a></li>
                                <li><a href="https://first1right.com" target="_blank">First1Right</a></li>
                                <li><a href="https://igandg.co.uk" target="_blank">Ideal Glass</a></li>
                                <li><a href="https://plumbers-hampshire.com" target="_blank">Plumbers Hampshire</a></li>
                                <li><a href="https://plumbing-farnham.com" target="_blank">Plumbing Farnham</a></li>
                                <li><a href="https://penn-lawn-mowers.co.uk" target="_blank">Penn Lawn Mowers</a></li>
                                <li><a href="https://hopkinselectrical.com" target="_blank">Hopkins Electrical</a></li>
                            </ul>
                        </div>

                        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h5>Services</h5>
                            <ul className="services-list">
                                <li><Link href="/services/search-engine-optimisation/"><a>Search Engine Optimisation</a></Link></li>
                                <li><Link href="/services/search-engine-optimisation/local-seo/"><a>Local Marketing Campaigns</a></Link></li>
                                <li><Link href="/services/wordpress-development/"><a>WordPress Development</a></Link></li>
                                <li><Link href="/services/ecommerce/"><a>E-Commerce Development</a></Link></li>
                                <li><Link href="/services/pay-per-click/"><a>Pay Per Click</a></Link></li>
                                <li><Link href="/services/search-engine-optimisation/seo-audits/"><a>Website SEO Audits</a></Link></li>
                                <li><Link href="/product/website-security-audit/"><a>Website Security Audit</a></Link></li>
                            </ul>
                        </div>

                        <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h5>Technologies We Use</h5>
                            <ul className="technology-list">
                                <li><Link href="/technologies/amazon-web-services/"><a>Amazon Web Services</a></Link></li>
                                <li><Link href="/technologies/woocommerce/"><a>WooCommerce Development</a></Link></li>
                                <li><Link href="/technologies/amazon-web-services/"><a>Amazon Web Services</a></Link></li>
                                <li><Link href="/technologies/javascript/"><a>Javascript Development</a></Link></li>                
                                <li><Link href="/technologies/php/"><a>PHP Development</a></Link></li>
                                <li><Link href="/technologies/symfony/"><a>Symfony Development</a></Link></li>
                                <li><Link href="/technologies/laravel/"><a>Laravel Development</a></Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="copyright text-center">
                <div className="container">
                    Website &copy; Lacey Tech 2005- 2021
                </div>
            </div>
        </footer>
    )
}

export default SiteFooter
