import React from 'react'

function Navbar({ children }){    
    return (
        <>
            <nav className="navbar">
                {children}
            </nav>
        </>
    )
}

export default Navbar
