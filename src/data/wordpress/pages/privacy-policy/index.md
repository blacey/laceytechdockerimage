---
title: "Privacy Policy"
date: "2012-07-10"
---

The use of the Lacey Tech Solutions website ([https://laceytechsolutions.co.uk](https://laceytechsolutions.co.uk)) is possible without any indication of personal data; however, if a data subject wants to use special services via our website, processing of personal data could become necessary. If the processing of personal data is necessary and there is no statutory basis for such processing, we generally obtain consent from the data subject.

Our company would like to inform the general public of the nature, scope, and purpose of the personal data we collect, use and process. The processing of personal data shall always be in line with the GDPR, E-Privacy, and following the country-specific data protection regulations applicable to Lacey Tech Solutions.

We have implemented numerous technical and organisational measures to secure and protect personal data that is processed through our website. However, Internet-based data transmissions may in principle have security gaps, so absolute protection may not be guaranteed.

## Definitions

Our data protection declaration should be legible and understandable to the general public, as well as our customers and business partners. To ensure this, we are going to explain the terminology used.

**Personal data** - Personal data is any information relating to an identifiable natural person. An identifiable natural person is one who can be identified (directly or indirectly), by reference to an identifier such as a name, identification number, location data or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person

**Data subject** - Data subject is any identified or identifiable natural person, whose personal data is processed by the controller responsible for the processing

**Processing** - Processing is the activities that are used on personal data. It doesn't matter whether or not this is done manually or by automated means

**Restriction of processing** - Restriction of processing is the marking of stored personal data with the aim of limiting their processing in the future

**Profiling** - Profiling is any form of automated processing of personal data. Profiling personal data is done to evaluate certain personal aspects relating to a natural person. Profiling can be used to analyse or predict aspects concerning that natural person

**Pseudonymisation** - Pseudonymisation is the processing of personal data in a way that the personal data can't be linked to an identifiable person, without the use of additional information. Provided the additional information is kept separately and is subject to security measures to protect the data

**Controller** - The Controller (or controller responsible for the processing) is the natural or legal person, public authority, agency or another body that determines the purposes and means of the processing of personal data

**Processor** - Processor is a natural or legal person, public authority, agency or another body which processes personal data on behalf of the controller

**Recipient** - Recipient is a natural or legal person, public authority, agency or another body where the personal data has been disclosed. Public authorities that receive personal data in the framework of a particular enquiry shall not be regarded as recipients; the processing of those data by those public authorities shall be in compliance with the applicable data protection rules according to the purposes of the processing

**Third-party** - Third-party is a natural or legal person, public authority, agency or body other than the data subject, controller, processor and persons who, under the direct authority of the controller or processor, are authorised to process personal data

**Consent** - Consent of the data subject is any freely given, specific, informed and unambiguous indication that the data subject signifies agreement to the processing of personal data relating to them

## Name and Address of the Data Controller

The data controller for Lacey Tech Solutions is:  
Ben Lacey  
Briarleas Court  
North Camp  
Farnborough  
Hampshire  
GU14 6HL

Email: [privacy@laceytechsolutions.co.uk](mailto:privacy@laceytechsolutions.co.uk)

## Collection of general data and information

This website collects a series of general data and information when you or an automated system views pages on our site. This general data is stored in the web server log files and does not include personal data.

### What General Data Is Collected?

- the web browser types and versions used
- the operating system used by the device visiting the website
- the website from which an accessing system reaches our website (referrers)
- the date and time of access
- the IP address of your device
- the Internet Service Provider of the accessing device, and
- any other similar data and information that we will need to investigate possible attacks and breaches on our systems

### Why do you need this general data?

When using these general data and information, Lacey Tech Solutions does not draw any conclusions about the data subject. We collect general data so we can:

- deliver the content of our website correctly
- optimise the content of our site and any advertisements used
- ensure the long-term stability of our computer systems, hosting servers and website technologies, and
- provide UK or EU law enforcement with the information necessary for criminal prosecutions in the event of a cyber attack

Lacey Tech Solutions analyses anonymously collected data with the aim of increasing security, and to ensure an optimal level of protection for the personal data we hold. The anonymous data of the server log files are stored separately from all personal data provided by you.

There are multiple layers of security that we use to protect our clients who host their websites with us. We use a VPN service whenever we connect to the Internet. Using a VPN means that any information sent or received over your connection is encrypted.

## Cookies

Our site uses cookies to provide a better user experience for our website. You can read our [cookie policy](/cookie-policy/) to see how we use cookies.

## Services We Use

\[sub-pages image=true excerpt=false columns=3\]

### How Long Will Personal Data Be Stored?

**Prospect Enquiries** - These are emails that have been sent through our contact form. All prospect enquiries will be stored for 1-year, after which they will be deleted

**Customers** \- We keep customer data for the length of your contract with us. If you continue your services, we will hold your data for 5-years to comply with local laws, after which they will be deleted unless necessary for on-going services

**Suppliers** - We keep supplier data on file for the time that they are providing their services. If they continue to provide the service, we will hold your data for 5-years to comply with local laws, after which they will be deleted unless necessary for on-going services

**Analytics** \- We keep anonymised website statistics for 36-months

### Routine Erasure of Personal Data

We shall store and process your personal data only for the period necessary to achieve the purpose. At the end of each year, we review what data we hold and how long we should keep that data to comply with UK and EU law.

## How Do We Secure Personal Data?

- We use a Virtual Private Network (VPN) on all devices that are used for business. The VPN encrypts any data that is sent or received on the device
- Our email system uses security to verify that messages sent from our domain have come from authorised email servers
- We auto-generate long and complex passwords for all online accounts. Our passwords are changed frequently and if an account is compromised the password is changed immediately
- There are hacker prevention methods deployed as part of our website hosting service. These methods add additional security to our web and database servers
- Communication between servers is done so on a secure connection and all websites use sFTP (secure FTP) to transfer files
- All devices used for business purposes have Ad-Blockers, Anti-Virus, Anti-Malware, Anti-Spyware and Anti-Adware protection
- We run daily automated scans on all devices for Viruses, Malware, Spyware and Adware
- Devices that are taken off-site are encrypted and we have the ability to remotely erase all data
- Business files are stored on an encrypted NAS (network attached storage) using FreeNAS. This encrypts the drives requiring an unlock passphrase to decrypt the data
- Wherever possible we use SSL security certificates to encrypt data
- Some online accounts allow 2-factor authentication, where a username, password and unique code is required to login
- Any printed documents are locked away in our safe or stored in a lockable filing cabinet

## Newsletter Subscriptions

On our website, visitors are given the opportunity to subscribe to our newsletter. We share company news and offers with our subscribers - please read our [newsletter policy](/privacy-policy/newsletter/) for more information.

## Contact Forms

Our website contains information that enables users to contact us, and we display our postal address, telephone and email address on our contact us page.

If you contact us using our contact forms then your enquiry is processed by the web server and forwarded on to our team. Your enquiry will be stored on our systems for 1-year and we use your information to assist with your enquiry.

We use a contact relationship management (CRM) system and all enquiries from the website are securely sent to this system. Our CRM is used by our sales team to manage and respond to enquiries. We store your enquiry for 1-year, and it is then deleted.

## Subscribing to Blog Comment Updates

You can subscribe to receive emails when follow-up comments are made on our blog articles. You do not need to subscribe in order to leave a comment. If you want to subscribe to get an email with follow up comments then we will send a confirmation email to check it was you that subscribed. You can revoke your comment updates subscription at any time.

## The Legal Basis For Data Processing

If the processing of personal data is necessary for the performance of a contract to which you are party to. For example, when processing operations are necessary for the supply of goods or to provide any other service, the processing is based on Article 6 of the GDPR.

The same applies to such processing operations which are necessary for carrying out pre-contractual measures, for example in the case of enquiries for our products or services.

In rare cases, the processing of personal data may be necessary to protect your vital interests or those of another natural person. For example, if you had an injury at our company we would have to pass on your name, age and other essential information on to a doctor, hospital or other third parties if you were unable to yourself.

Finally, personal data can be processed under legitimate business interests. Legitimate interests are only applicable if the interests or fundamental rights and freedoms you have require protection of personal data.

For example, if you are one of our customers, then we can contact you legitimate business interests if we want to offer additional services that may interest you.

## Automated Decision Making

We do not use automatic decision making or profiling of personal data.
