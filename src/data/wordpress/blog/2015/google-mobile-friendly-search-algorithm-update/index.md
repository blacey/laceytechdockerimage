---
title: "Get mobile friendly by 21st April 2015"
date: "2015-03-30"
categories: 
  - "seo"
tags: 
  - "google-algorithm-update"
  - "mobile-friendly"
  - "mobile-websites"
  - "seo-2"
---

Have you ever clicked on a Google search result on your tablet or mobile phone only to find yourself looking at a page that you had to horizontally scroll or pinch zoom to read the content? This situation usually occurs when the website you're viewing isn't mobile friendly.

For your end users browsing on a mobile phone it can be frustrating to visit a website that is not optimised for mobile viewing. To this end Google introduced a "mobile-friendly" label that appears next to mobile optimised websites in the search results.

![Google Mobile-Friendly Search Result](images/mobile-friendly.jpg)

In the early days of smartphones, there were critics who claimed they were just a gimmickÂ and people would not rely on them to search or browse the Internet due to small screen sizes and difficult interfaces. But the trends of the past several years have proven them wrong.

Google already considers **mobile usability** as part of its ranking calculations. On April 21st Google have announced they will be expanding their use of mobile-friendliness as a ranking signal.

This change will affect mobile searches in all languages worldwide and will have a significant impact for websites in Google search results. Consequently, users will find it easier to get relevant, high quality search results that are optimized for their devices.

In the past most of Google's algorithm updates (Panda, Penguin, Hummingbird and Pigeon) have been data refreshes that hold little bearing on existing search rankings. Zineb, (a member of the Google Webmaster Trends team) quoted at the SMX conference saying that the new mobile-friendly algorithm will have a big impact on search rankings. The latest changes to theÂ ranking algorithm will affect mobile searches in _ALL languages worldwide_.

If your website is not optimised for mobile then it should be moved to the top of your to-do list, especially if you rely on your website as your primary source of marketing.

> "Zineb from Google did not specifically release a percentage of queries impacted. It is also believes that about 50% of all searches done on Google are on mobile devices. The big takeaway from this is that if your site is not mobile-friendly, get to work now on it. If 50% of your traffic from Google comes from mobile devices, it sounds like if you are not mobile-friendly that virtually all of that traffic from mobile is at huge risk."Search Engine Land - [Google's mobile friendly algorithm is larger than Panda or Penguin](http://searchengineland.com/how-large-is-googles-mobile-friendly-algorithm-larger-than-panda-or-penguin-217026)
> 
> Search Engine Land - [Google's mobile friendly algorithm is larger than Panda or Penguin](http://searchengineland.com/how-large-is-googles-mobile-friendly-algorithm-larger-than-panda-or-penguin-217026)

## How does Google know if my website is mobile friendly?

A website is deemed, "mobile-friendly" if it meets the following criteria as detected by Google Bot:

- Your website used text that is readable without zooming
- Your website uses technology that works on a handheld device (things like Adobe Flash would stop you being listed as a mobile-friendly website)
- The content of your website automatically resizes to the screen width so you don't have to horizontally scroll or zoom in to view content
- You've placed links far enough apart so that the correct one can be easily tapped without issue

With people making the switch from desktop computers to tablets its never been more important to optimise your website for handheld devices. The way we use the Internet has changed considerably over the last 10 years and it is important for businesses of all sizes to move with the times.

## Is a mobile website worth the investment?

First and foremost any business owner has to think about their customers - after all, without them you don't have a business. Most small businesses will have a website and for the most part, this will be their only marketing tool online.

With more people using smartphones and browsing the Internet on the go it is important you make their lives as easy as possible. If your website is mobile-friendly they can access the information they need in a quick and easy manner then they are more likely to contact you.

If your website is not optimised for mobile devices your customers will get frustrated having to zoom in and having to scroll to read your content. They are more likely to leave your website because it is hard to use on their device - resulting in a potential loss of business.

Google even began testing non-mobile friendly icons in the search results pages in an effort to dissuade users from visiting sites that aren't mobile-friendly. This could be a big blow to high-quality websites that haven't moved to a mobile-friendly design.

## Improving Your Rank in Mobile Search Results

We've already seen how [Google is using SSL as a ranking factor](/blog/google-uses-ssl-as-a-ranking-factor/ "Google now uses â€˜Always On SSLâ€™ as a ranking factor") in its algorithm to rank secure websites higher than others in the search results. SSL can be expensive to setup so making your website mobile friendly is a good first step to modernise your website.

After optimising your website for mobile there are a number of things you can use to boost your rank even further for mobile searches:

- **Improve your page load times** - Mobile devices use 3G and 4G to load web pages and despite positive strides forward in mobile Internet services you need to make sure your website is as quick as possible.
- **Keep plenty of content on your pages** - Mobile users need things quickly and it may be tempting to reduce your on-page content, but your website still needs to rank in search engines. Content is still one of the most important ranking factors so keep as many words on the page as you can to amount of content Google can crawl.
- **Avoid modal boxes and pop-ups** - Modal boxes for website logins are approproate for larger screens but they can be a bit unusable on a mobile phone. Its best to link people to a page than to load content into a modal/popup box on handheld devices.

## Mobile Testing Tools

Google has a tool that will allow you to see how your website looks on mobile devices. Feel free to check out the [Google Mobile Testing Tool](https://www.google.com/webmasters/tools/mobile-friendly/) or [MobileTest.me](http://mobiletest.me/) to see how mobile visitors see your website.

If you have any questions feel free to leave a message in the comments section below. I hope to see more local businesses making the switch to mobile-friendly websites in the future.

If you would like assistance optimising your website for mobile please [get in contact](/contact/ "Contact").
