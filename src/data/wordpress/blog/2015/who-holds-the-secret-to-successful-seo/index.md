---
title: "Who holds the secret to successful Search Engine Optimisation?"
date: "2015-03-13"
categories: 
  - "seo"
tags: 
  - "seo-2"
  - "social-media-2"
---

Who holds the secret to successful SEO? If you follow blogs and websites that cover the topic, you could be forgiven for thinking that it is all in the hands of algorithms, robotic content writers and Google's unique magic, but SEO is a lot more involved than most people realise.

So what is the secret to amazing, effective and interesting web content that utilise SEO techniques? The answer is ... _**people**_.

Too many SEOs set out to try and appease search engines when the real focus has (and always should be) to your potential customers. As Cyrus Shepard over [at Moz](http://moz.com/blog/seo-myths) points out, people - not robots, Google's magical algorithm or fairies - are the driving force behind successful SEO content.

As he succinctly states, "_what's forgotten in this equation is that Google and other search engines strive to mimic **human behavior** in evaluating content (and no human wants to sort through a million near-duplicate pages) and use human generated signals (such as links and engagement metrics) to crawl and rank results_."

If Google is attempting to mimic the behaviour of a human user, then it makes sense that intuitive, gifted writers are the ideal experts in SEO. Those who can write highly ranked web content in a smooth, human way that is enjoyable to read - those are the true geniuses behind successful internet marketing. Because really, if you aren't doing something well, why bother doing it?

Turns out - there are a lot of people out there who don't know much about Search Engine Optimisation, but continue to tout themselves as SEO wizards. The best way to see how good an SEO consultant or company are you need to look at the results.

A comment from the Shepherd article linked above stood out to me as I was researching this post. [Tony Dimmock](http://moz.com/community/users/111727) aptly observed, "_there's way too many 'popular' online publications that publish guest posts from people who frankly know jack about SEO_."

## Rising Stars In The SEO Industry

So now that we have ruled out those people who "frankly don't know jack" - who _is_ worth paying attention to in the world of SEO professionals? In this article I have profiled three of the people that you need to know.

### Kristi Hines

![Kristi Hines](images/kristi-hines.jpg)

A rising star in the world of SEO, Kristi began like most of us - as a blogger maintaining a [regularly updated site](http://www.kikolani.com) packed with targeted info about content marketing, search engine optimisation, social media, and web analytics.

She has gone to great lengths to remain at the top of her game, and her writing is really enjoyable to read. She is HubSpot Inbound Marketing Certified and Google Analytics Qualified Professional. What's more is that she has pretty massive social media followings for someone you may never have heard of - 55K on Twitter, 29K on Google +, and 10K on Facebook.

Kristi's skills are pretty amazing, and her resume is impressive. She has consulted and written for the blogs of big name clients such as American Express, Capital One, FreshBooks and Bigcommerce. In her own words, her "_freelance writing portfolio contains over 400 posts that have generated a total of over 300k social shares and over 150k clicks from Google search visitors each month._" She's someone you want to watch - and learn from - in upcoming years.

* * *

### Rand Fishkin

![Rand Fishkin](images/rand-fishkin.jpg)

While many of us started messing around with the internet back in the early '90s, but not all of us have become mavens in the field. Jokingly referring to himself as "The Wizard of Moz," Rand Fishkin is a force to be reckoned with in the world of smart, well written SEO content.

He is the co-author of [two books](http://www.amazon.com/s/ref=dp_byline_sr_book_1?ie=UTF8&field-author=Rand+Fishkin&search-alias=books&text=Rand+Fishkin&sort=relevancerank) written on the topic of successful online marketing, and co-founded numerous highly ranked websites, such as the aforementioned Moz and [Inbound.org](http://inbound.org/).

Rand Fishkin contributes to the SEO community by blogging and producing videos to highlight the ever changing advances within the industry. He produces weekly videos for the public in his, [Whiteboard Friday](http://moz.com/blog/category/whiteboard-friday) series, which I thoroughly recommend watching. The videos are packed with useful information that is easy for non-technical audiences to watch and learn from and they are always high quality.

Rand is a great example of the heights that a fledgling scribe can reach with dedication, hard work and a keen intuition for what works - and what doesn't. In his own words, "_I've been invited to speak to the teams at Google, Facebook, & Microsoft... and given presentations for the United Nations, [Stanford University](http://www.seomoz.org/blog/a-college-dropout-speaking-at-stanford), [Public Media](http://www.seomoz.org/blog/public-media-2007-conference-wrapup), NPR... My travels have taken me from Beijing to Stockholm, Toronto to Milan, New York, London, Lima, Sofia, Cape Town, Sydney & many more."_

* * *

### Vanessa Fox

![Vanessa Fox](images/vanessa-fox.jpg)

Perhaps it is Vanessa Fox's former experience working for Google that gives her a unique understanding of the best way to write and post content. As the creator of [Google Webmaster Tools](http://www.google.com/webmasters/), she is arguably one of the most knowledgeable people on the planet in matters relating to this topic.

Her book, the universally well reviewed "Marketing in the Age of Google, Revised and Updated: Your Online Strategy IS Your Business Strategy" is a must-read for anyone who cares about their rankings.

In 2013, Vanessa sold her immensely successful company, Nine by Blue [described by Fox](http://www.geekwire.com/2013/vanessa-fox-sells-search-marketing-startup-blue-rkg/) as "_a robust enterprise-level search analytics platform"_ to Virginia-based search marketing firm RKG, and took a position as their chief product officer. Her current side project is [Keylime Toolbox,](http://www.keylimetoolbox.com/) a start up that "_provides SEO and user-focused online metrics and education for companies of all sizes."_

These are only three of the dynamic, dedicated individuals who have changed - and continue to change - the world of online marketing, effective content and SEO on a daily basis. I will be sure to add more names as the year progresses, as more and more business owners are realising that they can't do business without an SEO professional hiding in the wings.

As you can see, when SEO guidelines are followed you can get great results. Have a look at the [case studies](/case-studies/ "Case Studies") I've been involved in to see what a difference it's made to local companies.
