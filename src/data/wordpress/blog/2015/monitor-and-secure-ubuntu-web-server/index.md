---
title: "How To Monitor And Secure Ubuntu Web Server"
date: "2015-05-28"
categories: 
  - "website-hosting"
tags: 
  - "server"
  - "ubuntu"
---

Businesses of all sizes rely heavily on the uptime availability of their website and online services. If your website or server gets hacked or a critical piece of hardware fails; the downtime this causes can mean a significant loss of business.

In this article I will be guiding you through how to setup basic server monitoring and security set up for a web server running the Ubuntu operating system. A lot of the tools mentioned in this article can be installed on other Linux operating systems but this guide is focused on Ubuntu Linux.

## Why Is Server Monitoring Important?

Using a combination of tools you can be alerted to potential issues before they become a problem. If you are responsible for managing 1 or more servers its crucial that you continually monitor, update and fix security vulnerabilities quickly.

## Before We Begin

This guide is meant as an introduction into the world of server monitoring and security - I would urge you to read up on these topics and use your own judgement on what security measures you need for your set up.

I recommend making a full server backup before installing any of the tools listed in this article and would urge caution if you are working on a live (production) server.

In this article I will be editing a number of configuration files using the command line. My editor of choice is nano, which works well for me but there are others that you can choose from. If you are unfamiliar with this text editor please read the [Beginners guide to the nano text editor](http://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/) article over at How to Geek before continuing.

## Let's Get Started!

### DenyHosts

DenyHosts keeps track of invalid SSH (Secure Shell) login attempts and will happily block IP addresses that have failed to login successfully. To bolster security you can configure DenyHosts to download a list of known IP addresses from the central database that have been blocked by other servers using the DenyHosts application. In turn you can choose to upload IP addresses that have been blocked on server to the central database.

#### Install DenyHosts

You will need to connect to your server using SSH (Secure Shell) and issue the following command to install Logwatch on your server:

sudo apt-get install -y denyhosts

After running the above command, DenyHosts will be installed and ready to configure on your web server.

#### Configure DenyHosts

1. **Whitelist IP Addresses:**  
    After you install DenyHosts its important that you whitelist your IP address to avoid you accidentally blocking yourself from your own server. To do this open the following file:
    
    sudo nano /etc/hosts.allow
    
    Under the description, add in any IP addresses that cannot afford to be banned from the server; you can write each one on a separate line as follows:
    
    sshd: 123.456.789.120 sshd: 123.456.329.38
    
    You can use a service like [whatsmyip.org](http://www.whatsmyip.org/) to find out your public IP address so you can add it to the whitelist file.
    
2. **Set the block conditions for the ROOT user:**  
    I would always advise against logging into your server using the root account. Ideally you would login using an RSA authentication key, which allows to you securely login to the server without entering your password but that isn't covered in this tutorial.If you have disabled SSH root login on your system then you can safely change `DENY_THRESHOLD_ROOT` to `DENY_THRESHOLD_ROOT = 1`. If someone tries to login as the root user and they are unsuccessful they will be blocked.
3. **Set the block conditions for invalid users:**  
    This setting is rather useful as scripts will often try random usernames or service names to try and login to the system. Fail2Ban (see next step) will help us defend against brute force attacks but for the script kiddies who want to try their luck we'll let DenyHosts deal with them. All administrators for the server will know their user login - any other requests are likely to be hack attempts. Set the deny invalid threshold to 1 to block these requests.
    
    DENY\_THRESHOLD\_INVALID = 1
    
4. **Set DenyHosts to download a list of blocked IPs from other servers:**  
    This is optional, but its something I would recommend to further restrict access to the server. To set this up change the following configuration option:
    
    SYNC\_DOWNLOAD = YES
    
5. **Restart DenyHosts for your changes to take affect:**
    
    sudo /etc/init.d/denyhosts start
    

### Fail2Ban

Fail2Ban is a service that monitors your server for brute force login attempts. It does this by monitoring the server's authentication log and automatically updating your Linux Firewall to ban malicious users from gaining access to your server. Most of the time you will see repeated log entries of failed login attempts that have been run against your server to try and gain unauthorised access to the server.

#### Installing Fail2Ban

You will need to connect to your server using SSH (Secure Shell) and issue the following command to install Fail2Ban on your Ubuntu server:

sudo apt-get install fail2ban

#### Configure Fail2Ban

The Fail2Ban configuration files can be found in the `/etc/fail2ban/` directory. The first file we want to edit is `jail.conf` but we can't edit this file directly because its modified by package upgrades.

You need to copy the file so we can make our changes to the configuration. To do this enter the following command:

sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

Now all we have to do is open our local configuration and make our changes, so lets open the file using the Nano text editor:

nano /etc/fail2ban/jail.local

The settings in the DEFAULT section are applied for all enabled Fail2Ban services.

1. **Make sure your local IP is whitelisted:**  
    Find `ignoreip` and change this to `ignoreip = 127.0.0.1/8`  
    
2. **Set the ban time for malicious users:**  
    The bantime is the length of time that a user will be banned after a failed login attempt. This is set to 600 seconds (10 minutes) by default, which is adequate for most people's needs but it can be easily be changed. Find `bantime = 600` and either leave the default bantime or change it to your required time limit  
    
3. **Set the ban conditions for malicious users:**  
    Fail2Ban will only block a user if they meet the criteria specified by the `findtime` and `maxretry` options. The maxretry variable sets the number of login attempts allowed by a user within a pre-defined space of time.  
      
    The findtime variable sets the login window. By default a user will be banned if they enter incorrect login details 3 times in 10 minutes.You can change this by finding (or adding if it doesn't exist in \[default\]) the following lines:
    
    maxretry = 3 findtime = 600
    
4. **Set your email address to receive alerts:**  
    This is optional but I would recommend it so you know what's going. Change the following to set up email alerts:
    
    destemail = yourname@domain.com sendername = Fail2Ban mta = sendmai
    
    The meta parameter configures what mail service will be used to send the email.

It's important to note that Fail2Ban's default blocking action is to use the Linux Firewall to reject traffic from the malicious user's IP until the ban time expires. In some cases you may want to increase the ban time to offer more protection.

### ClamAV

A web server is just like any other Internet enabled device in the sense that viruses and malware can infect the server. Most websites these days use Content Management Systems (CMS's) like WordPress, Joomla and [Drupal](/blog/drupal-right-platform-for-website/) \- these systems allow end-users to upload files from their device to their website. If the uploaded files contain a virus it has the ability to infect the server and cause major problems for your hosting clients.

#### Installing ClamAV Anti-Virus

You will need to connect to your server using SSH (Secure Shell) and issue the following command to install Clam Anti-Virus on your Ubuntu server:

sudo apt-get install clamav clamav-daemon clamav-freshclam

#### Update The Virus Definitions Database

The two commands listed below will restart Clam Anti-Virus (freshclam), which automatically updates the database and does a manual update of the virus definitions.

sudo service freshclam restart
sudo /usr/bin/freshclam

#### Set Up Scheduled Virus Scanning

Setting up automated scanning of your Ubuntu server needs to be done using a crontask. To create a crontask enter the following command:

sudo crontask -e

The -e parameter allows you to edit existing crontasks that have been set up on your system. The cronjob below will run a virus definition update and run a full scan of the server every 4 hours. This will only generate a report when infected files have been found on the server.

Clam Anti-Virus has an option to auto-remove infected files but this is not recommended. No Anti-Virus system is 100% accurate and you wouldn't want an important configuration file to be removed without you knowing!

0 \*/4 \* \* \* /usr/bin/freshclam --quiet; /usr/bin/clamscan --recursive --no-summary --infected / 2>/dev/null

The cronjob below will run a virus definition update and run a full scan of the server once a day:

@daily /usr/bin/freshclam --quiet; /usr/bin/clamscan --recursive --no-summary --infected / 2>/dev/null

### Logwatch

Applications create log files to keep a historical record of activities that have taken place on the server. Log files can be complex and difficult to analyse, especially if your server hosts a lot of websites.

When a problem occurs the first port of call is to look through the various log files and see if these give you any clues as to what has caused the problem. When you're dealing with live server, being able to quickly locate and analyse these log files becomes exceptionally important for finding a solution to a given problem.

Logwatch is an application that sends a short email digest of things that have happened on your server during a given timeframe. Logwatch scans the log files on your server and categorises the information based on the service name. The emails can be configured to show all running services on the server or a select few you want to monitor.

#### Installing Logwatch

You will need to connect to your server using SSH (Secure Shell) and issue the following command to install Logwatch on your Ubuntu server:

sudo apt-get install -y logwatch

#### Configure Logwatch

Now that Logwatch is installed we have to configure it to our specific requirements. If I'm changing a configuration file, I always make a point of backing up the original file just in case things don't go to plan.

To backup the configuration file enter the following command:

zip /home/blacey/logwatchconf.zip /usr/share/logwatch/default.conf/logwatch.conf

This will create a zip file of logwatch.conf inside my home directory, you can specify any location you want for the configuration backups but I find having them in one place is easier to manage.

Now that we have a backup, lets open the logwatch configuration file by running the following command:

nano /usr/share/logwatch/default.conf/logwatch.conf

1. **Change the email address that daily reports are sent to:**  
    Find `MailTo = root` and change 'root' to your email address
2. **Change the email address that the reports are sent from:**  
    Find `MailFrom = Logwatch` and change this to the email address you used in step 1
3. **Setting the date range for the Logwatch reports:**  
    The default is `Range = yesterday` but I prefer changing this to: `Range = today`. There are three date range options available, All (all available since sever set up), Today or Yesterday
4. **Set the report detail:**  
    The default is `Detail = Low` but I prefer changing this to: `Detail = High` so I get a detailed daily report. There are three detail options, Low, Medium and High so feel free to use each one and find what works best for you
5. **Set the applications and services you want to include in the report:**  
    The default is `Service = All` and this meets my needs but for some it can be a bit overkill.  
    To tame the service list you can either specify certain applications to monitor or you can show all services and exclude the applications you don't want:Specify a list of services you want to monitor:
    
    Service = http Service = sshd Service = sudo
    
    Show all services and remove the ones you want to see in the report:
    
    Service = All Service = "-pam" Service = "-dovecot"
    
    To see a complete list of supported services run the following command:
    
    ls -l /usr/share/logwatch/scripts/services
    

If you've reached the end of this tutorial you should now have a Ubuntu web server set up with a good level of protection and monitoring to survive in the real world. If you have any questions about this tutorial or would like to share tools you find useful please feel free to leave a comment below.
