---
title: "Are There SEO Benefits to Business Blogging?"
date: "2015-02-23"
categories: 
  - "seo"
tags: 
  - "blogging"
  - "business"
  - "marketing"
  - "seo-2"
---

Business blogging is becoming more common-place and each article improves your online presence. When everyone is competing for website traffic and leads you need unique content that helps your customers.

When speaking with clients I often find that business owners fail to understand, is their corporate website is an investment that needs to be maintained if you want it to be successful.

I've previously written about the importance of [ongoing website maintenance](/blog/why-do-websites-need-regular-maintenance/) but writing good content on a regular basis also needs to be considered. I see so many websites that consist of 10 - 20 pages with content that lacks focus and fails to rank well in the search engines. So how can you remedy this?

## Starting a Business Blog

Search engines are forever updating their search algorithm to make sure you get accurate and up-to-date content that meets your search criteria. If your website hasn't been updated in 6 months you'll find it pretty hard to rank on page 1 for your chosen keywords.

Blogging gives your business a platform to provide your visitors with unique content that not only keeps them informed about your products and services but also helps build trust and authority within your industry.

### Can Blogging Help Your Business?

This really depends on your industry and target audience. Having a blog on your website can help improve your rankings in the search engines, build credibility for your business and help you engage with potential customers.

It's important to note there is a big difference between hobbyist and business blogging. Your [business blog](/blog/) should not be used as a platform to rant about your competitors - it should be professional and cover articles that relate to your business and industry. Blog articles that help customers at different stages of the buying cycle work best at driving enquiries and sales when an effective call to action is used.

## Why Start a Company Blog?

There are many reasons why a business would want to start a blog. We blog as a means of helping current and prospect customers by answering questions or giving advice.

A secondary reason why we blog is to get traffic to our website that hopefully buys from us or learn something new. Below are a few reasons why you should consider starting a company blog.

### Blogging Drives Traffic To Your Website

People use search engines on a daily basis to find information, people, places, services, answers to questions - the list is endless! A large majority of users won't type your business name into a search engine - instead, they will enter what they are looking for. But how often do you edit your website? A product or service page isn't likely to change much so your website will consist of content that doesn't change much.

Search engines **love** new content - it shows your website is regularly updated and with new information. Having a blog is a useful way to get new content on your website and engage with your audience. Every blog post you write is another page that will be indexed by search engines, which gives your business more a better chance of appearing in the search results.

> "The truth is, when you create solid content focused around topics, you almost always receive far more (and oftentimes better) traffic from long-tail keywords that you didn't try to rank for."[Cyrus Shepard - SEO Moz](http://moz.com/blog/seo-myths)
> 
> [Cyrus Shepard - SEO Moz](http://moz.com/blog/seo-myths)

### Turn Website Visitors Into Sales and Enquiries

Providing information to your website visitors is great, but your primary aim as a business owner is to convert visitors into return customers. It's important that your blog posts have a clear call to action to prompt further engagement with your website or perform an action like contacting you or downloading a brochure.

You should focus on researching and writing in-depth blog articles because longer posts are proven to rank higher in search engines. You don't have to write a blog post every week but keeping your blog updated with articles is important.

One of Google's ranking factors relates to the freshness of information - by adding new content you are giving search engines another reason to rank your website higher in the search engine results.

You could provide an automated newsletter to your visitors every month to keep them coming back to your website. You will typically find that if people like the quality of your articles they are likely to bookmark your blog or come back to read more. This is why it is important to write good quality articles and think about who you are writing the article for.

### Improves your Authority as a Business

As I mentioned before, you should treat your blog posts as an educational resource for your potential customers. Search engines typically give higher rankings to websites that have good quality content.

If someone likes one of your articles they are likely to share it on social media or forum websites. This creates a link back to your website that can improve your website's authority, which in turn will increase your rankings.

### Blogging Provides Long-Term Results

A blog post might take you an hour or two to write, which might seem like a long time but once your article has been published, it is on the Internet until you remove it. You might get 10 visitors a day to start with but when you share it on social media or send it to your customers using a MailChimp email campaign there is a good chance you will generate sales as a result of consistent marketing.

If you don't then you might need to tweak the focus of the article but generally speaking, visitors will find your content and you will get traffic to your site through the blog. As time goes by, people will come across your articles through a search engine, social media post or from a site that has linked to your content.

Just like social media, blogging can seem like a lot of work for very little return, but if your business commits to writing regular blog posts and sharing this content across your marketing channels, the benefits and lead generation can be surprisingly positive.

## What Results Can I Get From Blogging?

Results will always vary from client to client, industry to industry but don't let that put you off. Below is a handful of page 1 results that have been obtained through business blogging.

- [Consistency in web design](https://www.google.co.uk/search?q=consistency%20in%20web%20design) (Lacey Tech Solutions)
- [Magento Cart Abandonment Tracking](https://www.google.co.uk/search?q=magento%20cart%20abandonment%20tracking) (Lacey Tech Solutions)
- [What is Lock Snapping?](https://www.google.co.uk/search?q=what+is+lock+snapping) (Lockrite Security)
- [Hardwood Flooring Trends](https://www.google.co.uk/search?q=hardwood+flooring+2015) (V4 Woodflooring)

The SEO ranking positions of the posts listed above have remained constant throughout numerous Google Algorithm updates and they continue to be a good source of inbound traffic to the website.

## How Do You Write Articles?

A lot of business owners I meet are already juggling several key tasks to keep their business running. Most of them don't have time to focus on a blog and it often gets out dated. I've fallen victim to the same issue but I would always suggest writing fewer posts and focus on quality content if you want your blog to succeed.

Search engines and users appreciate **high quality content** and these articles typically rank well over time. Longer articles are better but they require more time to cover everything. Mix it up and write shorter articles between 500 and 750 words, then occasionally write a longer post between 1250 and 2000 words.

One of the best investments I made was to buy **Dragon Naturally Speaking** and a [good headset from Sennheiser](https://amzn.to/33o1mtZ). This allows me to dictate blog posts and saves a lot of time on the first draft.

Another tip that helps me structure the blog posts is to use Google search for topic research. Enter your topic into Google and scroll down to the bottom. There you will find the "People Also Ask" section, that lists related searches that have been done.

All you have to do is use the people also ask data, your topic research and look at frequently asked questions. This will form the basis of your article and you can write (or dictate) a couple of paragraphs for each heading.

## What Can I Write About?

**Write about current news**  
You need to show prospect visitors that your business is up to date with what's happening within your industry. You might as well take what you have to do and use it as an opportunity to be a resource for such information and provide your take on events.

**Write about new products being introduced to your business**  
If you're launching new products to your company - your visitors are going to want to know what new products are available. Writing a blog post about the new products and their benefits is a good way to keep your customers in the know.

**Write about changes in your industry**  
Having a blog is a good way to establish your authority within your industry. If you write about changes within your industry your blog will start to become a valuable resource to your customers.

**Celebrate staff achievements  
**If a member of staff has passed an exam or training course then you could highlight that on the blog. This shows to a prospect customer that your team are qualified, which helps to build trust.

**Answer questions  
**You could look through Google and find frequently asked questions in your industry and provide an answer. You could write one article that answers the question in depth, or you could have, "Question Time" where you answer several questions.

Blogging is can be challenging and time-consuming, but the results are well worth the time investment. You should try and balance your articles into logical categories so people can find what they're looking for quickly and easily.
