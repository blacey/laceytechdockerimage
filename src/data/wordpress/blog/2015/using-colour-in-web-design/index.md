---
title: "What Colours Should You Use For Your Website?"
date: "2015-04-08"
categories: 
  - "web-design"
tags: 
  - "colour"
  - "web-design"
---

The psychology of colour is a widely studied field in which academics, artists and scientists attempt to determine which colours affect human behaviours in what ways.

While the field is somewhat controversial, many web designers have touted the benefits of certain colours for conversion success - but are they just the victims of wishful thinking? In this article I examine the impact of colour on the success of your website, how it can play an important role in your company branding and uncover the ways that it won't.

## Getting Started

Before you get started, you should ask yourself the following questions:

- Who are you expecting to attract to your site?
- Who would you _like_ to attract?
- What services and products are your displaying/offering?
- What do you want to do with your website? Promotion? Direct sales?

You can choose the best colours for your website when you think about your intended audience.

## Colour Psychology and Branding

Colour psychology is the science of how colours affect human behaviour and is only one branch of the broader field of behavioural psychology. This is not a field without its critics, as some scientists dismiss this completely due to the inability to conduct scientific research and gather statistical data. That said, it's undeniable that people are affected deeply by the colours around them - we all have favourite shades, tones we dislike, and lighting/colouring that make us feel happy or sad.

With this in mind, it is a logical conclusion that the colour schemes you choose to use in your company branding are incredibly important for determining feel, establishing price point, attracting your target demographic; not to mention for creating a strong, recognisable logo.

### How Can Colour Affect Website Calls To Action?

Website call to action (CTA) is a button or some type of graphic on a website meant to prompt a user to click it and continue down a conversion funnel - like adding a product to their cart or clicking through to contact you.

Here are some examples of how colour can impact on your calls to action:

![How button colours affect calls to action](images/e-commerce-colour-ideas.png)

Successful colour branding plays on established cultural references to suggest trust, fun, urgency and need. This can include (but is not limited to):

- **Red -** urgency; "stop right there", "look at this!"
- **Orange -** cheerful; unpretentious; affordable; practical
- **Green -** calming; evokes nature; reliable and down to earth
- **Brown -** comfort; old world values; ruggedness
- **Black -** authority; power; no-nonsense
- **Blue -** honesty; simplicity; easy-going and friendly
- **Yellow -** fun loving; family friendly; off-beat and zany
- **Purple -** regal; individualistic; inventive and creative
- **Pink -** romantic; youthful; compassionate and loving

Obviously, this is a very rough guide; you can probably think of times that the colour green does not convey nature and instead communicates toxicity. But as a general guide, it is important to remember that the colours above come pre-loaded with messages that your users will register, even if those are not the messages that you intend.

## Web Design and Colour

At this point, perhaps you have established the colours your brand should employ to best communicate your message. How should you incorporate them into your [website design](/services/website-design/ "Website Design")?

**The colours used in web design need to do several things at once:**

- They must look good, and be aesthetically pleasing to most users
- They must ensure that the navigation of the site is clear and easy to follow for average users
- They must hold a visitor's attention, and avoid jarring and unsettling colour clashes or busy contrasts
- Above all - they must lead toward conversion by subtly urging the user to follow the actions that the site owners have in mind
- Be accessible for individuals with colour blindness - remember, green on red (and other combinations) are nearly impossible for them to read.

### Background and Accent Colours

Every website features background colours and accent colours. The background colour should say something about your brand and your product, but it is the accent colour that can really have an effect on your conversions. A strong call to action will be even stronger if it is featured in an accent shade that works well with the background and also catches the eye.

For a long time, web designers believed that this accent colour solution was a 'one size fits all' and that red or yellow reigned supreme as the best option to increase clicks and conversion. We know realise that this is not entirely true - there is no "magical conversion colour".

> "In an appropriately titled study called Impact of Color in Marketing, researchers found that up to 90% of snap judgments made about products can be based on color alone (depending on the product)."entrepreneur.com - The Psychology of Color in Marketing and Branding
> 
> entrepreneur.com - The Psychology of Color in Marketing and Branding

While the accent colour _should_ stand out and be bold (you can see how red and yellow are often good choices here), they must also contrast to the colour scheme of the entire site. If you have a red branded site already, a red 'buy now' button is not going to stand out from the branding and may be overlooked by your visitor.

Remember - your base colour and your accent colour should vividly contrast from one another.

## Cultural and Demographic Factors

Where are you expecting most of your web traffic to come from a regional market, Britain-wide or even a global market? This is important to ascertain, as certain colours are laced with meaning that you may not expect or anticipate, and this can skew your branding in certain markets.

Orange may not be the best colour choice if you are hoping to target the Irish market; white symbolises death, decay and mourning in China, and some regions of America strongly associate yellow with cowardliness. While these may seem like quirky anomalies, studies show that culturally insensitive branding can have disastrous marketing consequences.

Your visitor demographics are another important factor in how your branding and website colours are perceived. A younger clientele may be attracted to bright, crazy colours splashed across the screen, while older adults may find this same pattern garish and hard to look at, even driving them away from your site in favour of one with more muted tones.

## So What Now?

It's easy to see that the study of the psychology of colour and how it affects people's online shopping habits is controversial. While many experts believe that while colour does affect our mood and impulses, there is definitely no 'one size fits all' solution for your brand.

If you are struggling to decide what colour scheme you should choose for your background and accents, it often pays to [speak with a professional](/contact/ "Contact") who can guide you through the best choices for your site.
