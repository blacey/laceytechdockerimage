---
title: "Microsoft Drops Internet Explorer in Favour of Spartan Browser"
date: "2015-03-28"
categories: 
  - "development"
tags: 
  - "internet-explorer"
  - "web-browser"
---

To date, Microsoft has developed 11 versions of their web browser Internet Explorer from 1995 through to 2013. Nearly Nearly 20 years later Microsoft will stop development of Internet Explorer and will launch a new web-browser code-named "Spartan", which will ship with Windows 10 later this year.

## A bit of history...

Up until 2001 Internet Explorer was bundled with the Microsoft Operating System as the default browser and in 2001 Microsoft were taken to court on charges of Monopolisation. Bundling Internet Explorer with the Operating System was alleged to have contributed to Internet Explorer's earlier success as every Windows user has a copy of Internet Explorer pre-installed.

It was further alleged that having Internet Explorer pre-installed was a direct attempt at restricting the market for competing web browsers. Microsoft stated that the pre-installation of Internet Explorer was the result of innovation and that Windows and IE were inextricably linked. They argued that consumers were getting all the benefits of the web browser free of charge.

It was further speculated that Microsoft had altered its API's (Application Programming Interfaces) to favour Internet Explorer over third party browsers. The court case ruled that Microsoft had to share its API's with third party companies but no clauses were put in place to change the source code of Internet Explorer. In August 2002 Microsoft announced it would make concessions towards the proposed final settlement.

## The start of the browser war

The BrowserChoice.eu website was created by Microsoft to allow users that had not made, or were unaware of, a choice to try other browsers, and thus comply with the European Commission's ruling.

![Browser Choice](images/Browser-Choice.png)

Internet Explorer once dominated the web browser marketplace and had more than 1 billion users but its success started to decline in 2003 when it allowed other browsers to be installed on its Operating System. Soon consumers were able to use web browsers from competing vendors such as Firefox, Google Chrome, Safari and Opera. In December 2014 Microsoft was no longer required to show the Browser Choice screen to Windows users so a simple message is now displayed on browserchoice.eu.

## Internet Explorer's browser usage in decline

The Google Chrome browser has overtaken Firefox as the second most popular browser, behind Internet Explorer, as early as February this year. Below are statistics from StatCounter showing the browser usage share of the top 5 web browsers between June 2013 and February 2015.

<table><tbody><tr><td><strong>Web Browser</strong></td><td><strong>Feb 2015</strong></td><td><strong>Dec 2014</strong></td><td><strong>Jun 2014</strong></td><td><strong>Jan 2014</strong></td><td><strong>Jun 2013</strong></td></tr><tr><td><strong>Internet Explorer</strong></td><td>13.09%</td><td>15.21%</td><td>26.4%</td><td>24.6%</td><td>25.4%</td></tr><tr><td><strong>Google Chrome</strong></td><td>50.15%</td><td>48%</td><td>48.7%</td><td>46.6%</td><td>42.7%</td></tr><tr><td><strong>Firefox</strong></td><td>11.56%</td><td>11.27%</td><td>19.6%</td><td>20.4%</td><td>20%</td></tr><tr><td><strong>Safari</strong></td><td>14.64%</td><td>14%</td><td>4.9%</td><td>5.1%</td><td>8.4%</td></tr><tr><td><strong>Opera</strong></td><td>3.79%</td><td>1.5%</td><td>1.4%</td><td>1.3%</td><td>1%</td></tr></tbody></table>

Source: [Usage share of web browsers](http://en.wikipedia.org/wiki/Usage_share_of_web_browsers)

In recent years Microsoft worked hard to turn things around with Internet Explorer 10 and 11 but despite their best efforts they confirmed that it's time to throw in the towel. They are launching a new web browser, code-named "Project Spartan", which is due to be released later this year.

## Why is Microsoft abandoning development of Internet Explorer?

With the upcoming release of Windows 10 comes the potential for a fresh start on many faults in the Microsoftâ€™s primary operating system. Along with the removal of this hybrid tablet/desktop experience, Microsoft has announced that they will be ditching Internet Explorer.

Due to the negative connotations of Internet Explorer, they've found it would best to start with a new web browser to remove the negativity that surrounds the name. It was reported that Windows Phone barely made a blip in the mobile marketplace, which where most consumers are browsing the web.

## What does this mean for Website Designers and Developers?

Spartan provides a more dependable, and discoverable experience with forward thinking features including the ability to annotate on web pages, a distraction-free reading experience, and integration of Cortana for finding and doing tasks online quicker.

A new rendering engine powers Spartan that Windows 10 already has built into the operating system. While this new rendering engine is great in theory, what does that mean for all previous web applications that were built to support all the way back to IE7?

Microsoft has thought through the ins and outs with Spartan, as they should if it is going to successfully replace the infamous Internet Explorer. Spartan is designed to implement a dual-engine approach, where it can intelligently switch between the new modern rendering engine and Trident; the older IE engine.

Furthermore, Microsoft is finally ready to play nicely with what were once unknown experimental CSS, HTML, and debugging techniques and languages, which is a welcome relief for web developers.

> "When we announced Project Spartan in January, we laid out a plan to use our new rendering engine to power both Project Spartan and Internet Explorer on Windows 10, with the capability for both browsers to switch back to our legacy engine when they encounter legacy technologies or certain enterprise sites.
> 
> However, based on strong feedback from our Windows Insiders and customers, today we're announcing that on Windows 10, Project Spartan will host our new engine exclusively. Internet Explorer 11 will remain fundamentally unchanged from Windows 8.1, continuing to host the legacy engine exclusively."Kyle Pflug - [MSDN Project Spartan Developer Workshop](http://blogs.msdn.com/b/ie/archive/2015/03/24/updates-from-the-project-spartan-developer-workshop.aspx)
> 
> Kyle Pflug - [MSDN Project Spartan Developer Workshop](http://blogs.msdn.com/b/ie/archive/2015/03/24/updates-from-the-project-spartan-developer-workshop.aspx)

## What do we know about the Spartan Web Browser?

At present very little is known about the Spartan Web Browser but there are a lot of speculation on what features the new browser will have. Below is a leaked screenshot of the Spartan Web Browser being used on a mobile phone:

![Spartan Browser Phone Screenshot](images/spartan-browser-phone-screenshot.png)

The IE Dev Chat team has confirmed that plugins and extensions will be making an appearance in their new browser, which similar to what Firefox and Chrome.

> **#AskIE** **@bdsams** **@jacobrossi** **@IE** Yes. We're working on a plan for extensions for a future update to Project Spartan.

### Other rumoured features include

- [Cortana Integration](http://www.windows10update.com/2014/10/new-windows-10-cortana-information-leaks/ "Cortana Integration")
- Inking Support
- Chakra Javascript Engine
- Voice Control
- Tab grouping
- Plugin and Extension support

> "In the coming months, swathes of IE legacy were deleted from the new engine. Gone were document modes. Removed was the subsystem responsible for emulating IE8 layout quirks. VBScript eliminated. Remnants like attachEvent, X-UA-Compatible, currentStyle were all purged from the new engine. The codebase looks little like Trident anymore (far more diverged already than even Blink is from WebKit)."Jacob Rossi - [Smashing Magazine: Inside Microsoft's New Rendering Engine](http://www.smashingmagazine.com/2015/01/26/inside-microsofts-new-rendering-engine-project-spartan/)
> 
> Jacob Rossi - [Smashing Magazine: Inside Microsoft's New Rendering Engine](http://www.smashingmagazine.com/2015/01/26/inside-microsofts-new-rendering-engine-project-spartan/)

The Spartan web browser may simply be a rebrand of IE with a redesigned user interface to take on the popularity of other browsers. Support for plugins and extensions will certainly help the Spartan browser gain popularity but it will take more than a face lift to win over the hearts and minds of end users.

## In Closing

With the rebirth of a new web browser, along with the shift from Windows 8.1 to Windows 10 it's an exciting time for Microsoft and their end users. My personal web browser of choice is Google Chrome and it will take quite a lot of convincing to get me to switch but I am hopeful that Spartan will address the needs of its users.

**What are your thoughts on the new web browser from Microsoft? Let us know in the comments below.**
