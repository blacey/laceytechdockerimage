---
title: "Using Google Advanced Segments"
date: "2011-03-31"
categories: 
  - "seo"
tags: 
  - "google-analytics"
  - "seo-2"
---

[Google Analytics](http://www.google.com/analytics "Google analytics") is a service that allows you to **monitor visits** to your website. When you create a Google Analytics account you are given a code that links your website with your account.

Google Analytics is brilliant for finding useful information about your website visitors; you can:

- Track how many people have visited your website
- Compare user behaviour or visitor conversion rates to previous months or years
- See what device they used to view your website (Computer, iPhone, iPad, Blackberry etc)
- There is a world map that shows you what countries the visitors live in
- Allows you to track the effectiveness of an online or offline marketing campaign
- You can generate custom reports and have these emailed to you automatically

## How Can Google Analytics Help?

[Google Analytics](http://www.google.com/analytics "Google Analytics") is a free service that allows you to track information about your website visitors that can help you make crucial business decisions regarding your web presence.

One of the most beneficial reporting tools available to you is being able to compare website traffic with previous times of the year. So if you had an Ad-campaign active for a month, you could see if there was any change in user behaviour or visitor conversion rates compared to the previous month.

Google gives you access to hundreds of data reports and graphs to visualise the data being stored for your website, making it nice and easy for you find out the information you need. Services like [Twitter](http://www.twitter.com "Twitter"), [Facebook](http://www.facebook.com "Facebook") and [Linkedin](http://www.linkedin.com "Linkedin") are brilliant for sharing information and communicating with other people on the Internet. 

Companies use a wide range of advertising methods to promote their company, brand, products and services, and Social Media allows them to engage with potential customers on a global scale so it's important that they know where their advertising efforts have been successful.

One of the more difficult things to do in social media is being able to **track the effectiveness of your campaign**, but hopefully by the end of this article you will know more about what Google Anaytics can do.

You would think that tracking Social Media traffic to your website would be straight forward, but with all the third party Twitter applications such as TweetDeck, Hootsuite and Tweetie your website traffic becomes harder to track.

Third party applications are not classified as web browsers so they don't have a referring URL, which Google uses to track where the visitor has come from.

When a user clicks on a link in one of these 3rd party applications (since they are not in a browser) they do not send any referring information.

When Google Analytics doesn't receive any referral information it puts all of these visits into direct traffic, which is misleading.

## Setting Up Google Analytics Segments

- Login to Google Analytics and open your website profile
- In the sidebar on the left hand side click on, 'Advanced Segments' in your main Google Analytics profile.
- Click on, 'Create new custom segment'. In the sidebar on the left there will be a green and blue list, click on, 'Traffic Sources' in the green list and click/drag the 'Source' option onto, 'dimension or metric' in the page (on the right under the heading, 'Out of a total of ? visits...'
- This will give you some new options. Under 'condition' use the drop-down menu to select, 'Matches Regular Expression' and copy/paste the list below into the 'value' box.digg | aim | friendfeed | econsultancy | blinklist | fark | furl | misterwongs | wikipedia | stumbleupon | netvibes | bloglines | linkedin | facebook | del\\.icio\\.us | feedburner | twitter | technorati | faves\\.com | newsgator | PRweb | msplinks | myspace | bit\\.ly | tr\\.im | cli\\.gs | zi\\.ma | poprl | tinyurl
- Click, 'Test Segment' to make sure things are working correctly.
- Give your segment a name and click, 'Save Segment'.

This will separate all the social media traffic to your website into a group (advanced segment) so you can see where your visitors have come from and what pages of your site they have viewed.

## Viewing The New Traffic Segment

- Login to your Google Analytics account
- On the right hand side under "My Analytics Accounts" you will see "Advanced Segments" with a drop-down. Click the drop-down link (this will open a new panel).
- Tick the "social media" segment you created earlier and click on apply

This will now change the chart to show your normal traffic in blue and traffic from social media sources in orange so you can compare them.
