---
title: "5 Time Saving Tips For Social Media Success"
date: "2016-10-31"
categories: 
  - "social-media"
---

For small business, time is precious so we've listed 5 time saving tips to help you get the most out of social media without it taking hours! Your Social Media profiles are fast becoming one of the most important ingredients in your recipe for success but so business owners don't have time for Social Media - but we're going to change that!

But is updating it becoming a bit of a chore? Have you signed up for various platforms and are now wondering what to do next? Perhaps you feel overwhelmed with all the jargon?

Whilst you can join Facebook, Twitter, Instagram, Tumblr and an ever growing list of other platforms for free, there is a cost in the amount of time you need to spend keeping all you social media platforms relevant and up to date.

It’s exhausting! In fact, a forbes.com article recently cited the average company can spend as much 32 hours a month managing a single social media profile! Surely at least 30 of those hours could have been better spent working at what you do best?

Don’t drown under the stress of it all, have a look at our guide that covers time saving tips to make updating your social media accounts easier.

## Use a social media scheduler

Websites like hoot suite, Tweetdeck or buffer allow you to create posts and schedule them to be published at a time to suit you. You can plan days, week or even months in advance. You can even bulk upload your tweets to save time on some platforms.

You should try to dedicate time to schedule in posts at the beginning of each month so you have a clear run focusing on your business. If you haven't used a social media scheduling tool before we hope our shortlist will get you started!

### Buffer

Buffer has made waves in the social media marketing industry for its ability to schedule recurring posts. That means you don’t need to reinvent the wheel every time you schedule the same piece of content. Buffer's free version only allows you to schedule posts for a single profile–and limits you to ten posts at a time. The free plan is best-suited for managing a single profile for a small business as there are a few limitations in what you can do.

### Hootsuite

One of the most recognisable names in social media management, Hootsuite is accessible to businesses of all size with a freemium offering. The free Hootsuite plan allows you to connect up to three social media profiles from a choice of Facebook, Twitter, Instagram, Google+, LinkedIn and Youtube.

### Tweet Deck

The tool allows you to post and schedule to your Twitter accounts, as well as stream all of your feeds in real time. It can be easily integrated with Buffer, for more detailed management. Tweetdeck is fully accessible for free, with no limits to the number of Twitter accounts used, however we recommend one account per Twitter profile.

## Choose your social media accounts carefully

Some social media platforms are better suited to certain types of businesses or industries. Facebook for example, tends to have a slightly older demographic of users so this could work well for businesses looking to promote higher end goods or services. Instagram on the other hand, has a much younger demographic who are won over with instant visuals and following trends.

There is no point having a profile for every social media platform as small businesses don’t have the time to keep them all updated. It’s important that you understand your ideal customer as this will help you know what social platform will yield better results. By knowing your target customers, you can and plan your social media campaign accordingly.

## Focus on quality, not quantity!

Remember this is your business campaign and not your personal social media profile. It might be fine to post loads of photos from a night out on your own profile, but your business profiles should have content that is suitable and relevant.

Some examples of suitable content could be:

- Sharing details of a special offer
- Informing your customers or a of a new product range
- Share quotes that are humorous, inspiring or motivational
- You can post relevant industry statistics
- There’s nothing wrong with recycling old blog posts
- You can create polls or ask basic questions that your followers can answer
- When you introduce a new product or service you should share photos to get followers commenting and sharing your status updates
- Ask for feedback on your products or services - your followers can give you suggestions to improve
- You could reflect on something you’ve learned or experienced during your work day
- If a member of the team is going on a training course or has won an award, it would be nice to share that with your customers
- A couple of posts that will engage with your audience not only saves you time, but will have a more positive effect than lots of little posts about nothing just to fill the feed

## Learn how each social platform can help you

Facebook is a good example of this. Did you know for example, that you can plan a paid advertising campaign that is targeted at a specific demographic - like men between 25 and 50 who enjoy golf or swimming. With some social media sites you can create events and invite followers to your new shop opening, new product launch or whatever event suits your type of business.

Before you rush off to create your social media accounts, its important that you work out what social media platforms are suitable for your business. You will find many high-street brands use social media as part of their marketing strategy – so why not look through their profiles?

You can see what posts entice your audience to leave a comment or share your content. Once you’ve made a note of what works well for customers in your industry you can use Buffer or Tweetdeck to schedule your own social media posts.

## Interact with your followers

Depending on the nature of your business, you may find that you receive at lot of comments on your posts. It is important that you take the time to reply where as soon as you can. A lot of big companies will reply to the first one or two posts and then let the comments roll on below.

It is important not to ignore your audience and to engage with them were you can. Remember, the trick is to not spend hours pouring over your social media accounts, so respond efficiently.

If your social media is really stressing you out, you can hire external marketeers that will take over the running of your social media on your behalf. There will, of course, be a cost involved, but this may be justifiable for your business.

So now you know how to take the stress out of social media, you sit back and enjoy the additional marketing and brand awareness benefits available to you via a wide variety of social media platforms.

If you’ve had success with some of the tips above then we would love to hear from you - so feel free to leave a comment below!
