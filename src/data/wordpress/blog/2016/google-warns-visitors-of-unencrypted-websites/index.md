---
title: "Google Warns Visitors of Unencrypted Websites"
date: "2016-12-21"
categories: 
  - "seo"
tags: 
  - "google-chrome"
  - "ssl"
  - "web-browser"
---

It is no secret that Google has been ramping up their efforts to secure the Internet and when they release version 56 of the Chrome Web Browser they will show insecure websites with a "non-secure" warning in the address bar, as seen below.

With online/cyber crime on the increase, Google are putting in updates to the search results and their web browser to encourage website owners to migrate their websites to SSL (HTTPS).

![](images/google-ssl-notifications-chrome.jpg)

## HTTP vs HTTPS

Websites are considered insecure if they allow passwords, credit card and sensitive information to be submitted. Google has been pushing websites to switch to SSL (encrypted) connections for years when it announced that [SSL use would be a ranking factor](/blog/google-uses-ssl-as-a-ranking-factor/) for search engine optimisation (SEO).

It is important that you setup SSL for E-Commerce and Content Managed Websites that use Joomla, Drupal, Magento or any other system that allows you to login and make changes to your website. This update is due to be released on Jan. 31, 2017 and it is another milestone in Google's plan to mark all HTTP websites as non-secure.

While the update in January 2017 will mark some websites as non-secure, we will soon see Google Chrome marking **all HTTP pages** with a red danger icon, which is currently shown when SSL encryption is broken.

Emily Schechter of the Chrome Security team said, "Chrome currently indicates HTTP connections with a neutral indicator. This doesn't reflect the true lack of security for HTTP connections. When you load a website over HTTP, someone else on the network can look at or modify the site before it gets to you."

## The importance of SSL security certificates

Without SSL encryption, your website could be vulnerable to malware injection or a full blown website hack. [A survey released in 2015](http://www.statista.com/statistics/267307/https-encrypted-traffic-increase-type-worldwide/) found that one third of website traffic was served over HTTPS. Thankfully, that figure has increased after the survey was conducted but there is still a long way to go before we see SSL everywhere.

> "A substantial portion of web traffic has transitioned to HTTPS so far, and HTTPS usage is consistently increasing. We recently hit a milestone with more than half of Chrome desktop page loads now served over HTTPS. In addition, since the time we released our HTTPS report in February, 12 more of the top 100 websites have changed their serving default from HTTP to HTTPS.
> 
> Emily Schechter, Chrome Security Team _\-_ [Quote Source](https://security.googleblog.com/2016/09/moving-towards-more-secure-web.html)

## Frequently Asked Questions

### **Is SSL (HTTPS) a new thing?**

Not at all, it’s been around since the beginning of the World Wide Web, however because of the added cost and complexity, traditional only online shops and sites handling sensitive data or with a specific need for security, have used them.

### **Is it easy to migrate websites over to SSL (HTTPS)?**

There are a lot of factors that can get in the way. Some website hosts make it easier than others, add to this some website platforms are easier than others. A further consideration is that when a site is migrated to HTTPS, this can often result in broken links that can impact your position within the search engine listings.

### **Is it easy to enable SSL in WordPress?**

With WordPress for example, migration is usually simple on smaller sites, however off-the-shelf themes aren’t always 100% compatible with running under HTTPS. Our team can help get your website working using SSL and ensure everything is working on your behalf.

### **Do you need SSL for your website?**

SSL is standard for any website built in 2019 and beyond. Any website that does not use https will show a "Not Secure" label in red next to the website URL. This can put customers off and you may see more people leaving your website because of this. For E-Commerce websites SSL security is a must - no exceptions - because you are storing customer data that needs to be protected. Just having an SSL certificate doesn't ensure protection of data, you need strong passwords and other security measures to lower your risk of a data breach or website hack.

Speak to our team using our [contact form](/contact/) or call us on **01252 518233** to discuss getting an SSL certificate for your website.
