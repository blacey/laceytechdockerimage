---
title: "Marketing Channels Small Businesses Use To Get Customers"
date: "2020-04-15"
categories: 
  - "seo"
tags: 
  - "marketing"
  - "small-business"
---

Is your small business struggling to get success with online marketing? This guide is perfect for you as we discuss [over 43 marketing options](#marketing-options)!

Small Businesses have a wide range of marketing options and most don't know what is suitable for their individual needs. There is print marketing, paid adverts, [search engine optimisation](https://laceytechsolutions.co.uk/blog/black-hat-seo-kills-your-website-search-rankings/), remarketing, social media, email marketing, voice search, [blog posts](/blog/10-ways-to-successfully-promote-your-blog/) and radio to name a few.

All these forms of marketing work, but businesses rarely take time to **plan their goals and underlying strategy** to see what will work for them.

If you are one of those people then keep reading!

## I've tried loads of marketing services and nothing worked!

We hear this a lot from small and medium-sized businesses, and it's not uncommon for businesses to try several marketing avenues without success.

![](images/worried-man.jpg)

**Are you in the same situation?**

What separates the winners from the losers is strategy planning combined with the right marketing message distributed using **the right marketing channel**.

Modern businesses have so many options, all promising great results that fail to generate new business. Most seduce businesses to pay for ads or to boost posts, knowing that the results wildly vary with little return on investment.

Regardless of what marketing channel you use, you should **always think about your target audience** as that will act as a compass, guiding your decisions on marketing spend.

## What Marketing Options Exist?

- Email Marketing
- Text Message Marketing
- Postal Mail
- Social Media
- Search Engine Optimisation
- Pay Per Click
- Voice Search
- Cold Calling
- Customer Referrals / Word of Mouth
- Newsletters
- Private Tender Lists
- Paid Online Advertising
- Promotional Clothing
- Local Networking Events
- Newspaper Adverts
- Event Sponsorship
- QR Codes
- Video Marketing
- Podcasts
- Presentations
- Giveaways
- Competitions
- Press releases
- Exhibitions
- Trade Shows
- Billboard Adverts
- Door to Door Sales
- Blog and Content Marketing
- Point of Sale Displays
- Radio Advertising
- Signage
- TV Advert
- Promotional Video Ad Campaign
- Awards
- Branded Stationery
- Leaflet Handouts
- Pop-up Stores
- Product or Service Collaborations
- Webinars
- Seminars
- Affiliate Marketing
- Wearable Technology
- Fundraising Event

## How do you choose relevant marketing channels?

![Choosing Marketing Channels for Small Business](images/choosing-markting-channels.jpg)

Choosing the right marketing channels for your business was often costly and required experts to get results. We always recommend seeking expert advice and having a marketing strategy in place. You can always do some of your own marketing if cash is tight, but at least talk with an expert first.

In our experience, it is vital that you think about the points shown in the list below. In order to know what marketing channels are working you need to be able to track and measure the success.

- Have a marketing strategy
- Clearly define the business goals
- Measure the success using key performance indicators
- Consider your target audience and their needs
- Know what options exist - not only now but in the future too

## Are there any cost-effective marketing options?

### Search Engine Optimisation

We have many [case studies](/case-studies/) showing that SEO works and can have a lasting effect. It does have its perks but too many people seem to be blinkered and choose it above other marketing options.

Yes, We all agree Search Engine Optimisation is a great long term marketing strategy to get traffic to your website. The downside is it takes time to cultivate and grow.

### Social Media

Potential customers use social media for different reasons, some want to keep in touch with family, others use it to get a daily fix of cute animal photos and others use it for customer support.

Social media is one of the biggest and easiest ways of getting a website noticed. If you have a site that is captivating enough it can travel the world overnight.

You can use marketing automation on social media to help with customer support and brand awareness. Businesses may decide to use a company for [social media management](/services/social-media-marketing/) as they lack the time to do it properly.

If you are a big brand you may want to create a dedicated social media account for customer queries and support. Smaller businesses need not worry as the business resources may be limited.

### Email Marketing and Text Messages (SMS)

Email marketing is another effective route into getting your business name out there. You can email your customers who are already interested in your services to let them know about other products and services.

> **_“59% of respondents say marketing emails influence their purchase decisions”_**
> 
> [HubSpot Email Marketing Stats 2019](https://blog.hubspot.com/marketing/email-marketing-stats)

If you build your customer database you can easily spread the word about company offers, news and advice. The key thing here is you can only send marketing emails if you have the recipients consent. We would go a step further and recommend only sending emails that are related to the service the customer is using.

With minimal costs to set up a newsletter or customer relationship management system, it is a great marketing strategy that can yield a good return on investment. Email newsletters can be emailed out on a schedule and can include blog posts from your website so it is always current.

SMS (text messaging) is great for reminding customers of appointments or latest offers. You need to get written consent before marketing to your customers.

**Please Note:** In May 2018 the GDPR (general data protection regulations) came into force, which gives more power to individuals in how their personal data is stored and processed.

If you buy email contact lists then you need to ensure that everyone on that list has given their consent to be contacted by you (a third-party). If you cannot guarantee that, we would suggest you don’t purchase the list as you could find yourself with an investigation from the information commissioner's office (ICO) along with a fine if you send marketing emails without consent.

### Blog Posts and Guest Posts

Following on from email marketing you have a blog and news articles. These can be a great way to let your customers know about issues in your industry, company news and Q&A content.

Blog posts help educate your customers and build trust. They remain on your website until you choose to delete it. Our blog generates over 60% of visits to the website on a monthly basis.

> **_“47% of buyers viewed 3-5 pieces of content before engaging with a sales rep.”_**
> 
> [Demand Gen Report, 2016](https://www.demandgenreport.com/resources/research/2016-demand-generation-benchmark-report)

Another option to **marketing your business** is Guest Posting. This is where you contact bloggers in your industry and ask if you can write a blog post for them.

You'll normally be asked to ensure the article fits in with their audience and you can often include a link to your website in the article.

This approach takes a lot of time but you benefit from your company being promoted to new and often larger audiences. We provide this service as part of our search engine optimisation service and it gets results.

### Video Content

![](images/word-image-2-768x1024.jpeg)

Videos are a great way to keep customers engaged with your content that results in them staying on your website for longer.

There are many different types of video content that can be created, shared and viewed online.

### What types of video can you create?

- Explainer or educational videos
- Product demonstration videos
- Customer testimonial videos
- Video live streaming
- Meet the team or behind the scenes videos
- Interviews / Q&A videos
- Announcement videos
- Case study results videos
- Presentations videos

With people often being short on time, video content allows your customers to easily understand something in a short amount of time. Using video in your marketing strategy could see an increase in purchases or enquiries.

> **_“93% of marketers say they’ve landed a new customer thanks to a video on social media. Not only do customers gain trust in companies, but they’re also able to educate themselves about the company or their product...”_**
> 
> [Oberlo - Video as a Lead Generation channel](https://www.oberlo.co.uk/blog/video-marketing-statistics#6_Video_Marketing_as_a_Lead_Generation_Channel)  

We recommend using free video hosting services such as YouTube or Vimeo. These services both have free use options and Vimeo has several paid plans available.

Using a video hosting service you will be able to see statistics related to each of your videos. Vimeo allows you to add links to pages or show a lead generation form in order to continue watching the video.

### Pay Per Click Advertising

Pay Per Click (PPC) can be a really fast way of getting potential customers to your website or landing page. PPC is efficient and ensures people who are looking for the information are directed to you.

Ideally, you would do keyword and customer research so you can **write compelling Ad content**. The better your Advert, the higher your trust score will become, leading to lower cost per click fees.

A [PPC campaign](/services/search-engine-optimisation/pay-per-click/) needs to be continually monitored and maintained, it takes time but if you aren't optimising then the cost can add up.

![](images/marketing-funnel-diagram.jpg)

Source: [https://www.bluecorona.com/blog/pay-per-click-statistics](https://www.bluecorona.com/blog/pay-per-click-statistics)

Unfortunately, the cost of each click is dependant on levels of competition, the quality of your ad text and number of conversions.

**_“The average person is served over 1,700 banner ads per month, but only half of them are ever viewed”_**

Pay Per Click Statistics 2019 - [https://www.bluecorona.com/blog/pay-per-click-statistics](https://www.bluecorona.com/blog/pay-per-click-statistics)

We don't advise managing your own Pay Per Click marketing as you can lose a ton of money if you don't approach things the right way. The main benefit of Pay Per Click is that it is quick to set up. With PPC you get feedback from the advert within an hour.

## Conclusion

With any marketing solution, there will always be positives and negatives to consider. Over the last 12 years, we have seen a massive shift in how people use modern technology to find information, products and services.

- **Do not rely on one marketing channel** to drive customer acquisition. This makes your marketing strategy vulnerable so using a combination of methods will be to your benefit. We have seen so many businesses fail because they didn't adopt a multi-channel marketing strategy.
- You should track and measure your marketing activities to track return on investment. You can then drop the marketing activities that yield little or no results.
- Having clear marketing goals will assist you when designing the landing page. Headings and content will be aligned with your goal and this will improve conversion rates.
- Use A/B split testing (or multi-variant testing) so you can test variations within your website and improve conversion rates. You can easily track and measure what variation was preferred among your website visitors.
- Try to collect and build a list of email addresses of people who are interested in your services or products. This will allow you to email past, prospective and new customers with relevant offers in the future.

We have seen how Google is using modern technology to help people find information online. The search results are continually improving and Google now presents information in a variety of ways.

No more is Google just a search engine with 10 search results, they now incorporate maps, news carousels, product carousels, movie times, train times and more.
