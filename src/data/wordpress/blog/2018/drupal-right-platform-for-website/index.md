---
title: "Is Drupal The Right Platform For My Website?"
date: "2018-05-21"
categories: 
  - "development"
tags: 
  - "cms"
  - "drupal"
---

When creating a website, more users, businesses, and admins are turning to Content Management Systems (or CMS). These tools allow you to dynamically add, manage, edit, and remove content from a site with minimal training.

No longer do you have to learn and tediously code HTML and CSS or pay a developer every time you want to update your website. There are quite a few CMS options to help you manage and update sites within minutes.

Today, we are going to look at Drupal, and go through why we think this project is worth considering.

## Why Should You Choose Drupal?

### Easy To Update

Content Management Systems are supposed to make it easier to build and update your website. Drupal allows you to create multiple back-end users, setting different permissions and different roles for your team.

Are there some parts you would prefer only your most IT proficient users get to play around with while you also have a content team that could benefit from being able to add new content without having to go through a gatekeeper.

For community-based websites, you can extend some editing capabilities to the front-end of the website, too.

### Extend Drupal With Modules

The strength of a CMS site-building tool for the layman user is how easy and efficient it makes the task of launching and updating a website. However, that ease is ruined if you’re trading too much versatility for it.

If you’re constrained too much in what you can actually do with your site, then you’re not going to be able to explore all the options that might get you more clicks, more conversions, more subscriptions, and so on. It seems like you have to trade variety for reliability at some point, but the modules system of Drupal allows you to maintain both.

![](images/Webp.net-resizeimage-7.jpg)

Using modules allows you to add all sorts of content and ease-of-use tools to your website, including tools made by the developers of Drupal themselves but also thousands made by a sprawling community of over a million users. There are modules for customer relationship management tools, for search engine optimisation, for social media integration and much more.

Just a look at some of the [top modules available](https://codegeekz.com/15-best-drupal-modules-and-plugins-for-developers/) gives a quick understanding of just how much can be accomplished. You can vary the site in all sorts of directions both from the developers perspective and the visitors perspective.

### Drupal Is Free For Commercial And Personal Use

Let’s start with some of the single best features of Drupal as a CMS. For one, it’s entirely free, so your site-building costs will immediately drop when compared to some of the "premium" options out there.

That doesn’t mean it’s lacking, either. As it is Open Source, Drupal has been able to grow a huge community of users that have all contributed their knowledge.

An experienced Drupal developer is not just using the suite as it was built by developers. They’re using a long list of tools developed by the community, too, or even building their own unique twists to your site. 

The fact that many of these tools already exist means that any problem or desire likely has a solution that just has to be found, rather than having a solution built from scratch.

The quicker a website can be effectively built, the less it’s going to cost to build that website. Just one look at some of the [best Drupal-built sites](https://www.awwwards.com/websites/drupal/) shows that you’re not sacrificing quality for expediency, either. There are all kinds of sites with all kinds of purposes and functions built using these tools.

### Choosing a Drupal Theme

It’s easy to get caught up in all the bells and whistles of website building, especially when such a vast number of different modules gives you so many options. However, the visual side of web design is just as important.

There are hundreds of different aesthetic styles you can get from sites like [ThemeForest](https://themeforest.net/category/cms-themes/drupal?ref=blacey) or you can work with a graphic designer to create one that fits your website specifically.

![](images/Webp.net-resizeimage-8.jpg)

Themes built for Drupal are responsive, meaning they can fit your design choices no matter how you tend to use them or on what device they are viewed. Many have adjustable colour schemes, choices of stock imagery, layout editors, and more to help the visuals better fit your brand.

### An Eye for Metrics

Data is becoming more and more important online. Businesses use their website data to better understand their customers and their internal processes. Data can also be used by just about any website owner to see what works, what doesn’t, and how their visitors use the website.

Drupal is built with data in mind and with modules allowing you access to website usage metrics that provide insights to understand and improve the website.

### Community Driven Support

One of the fears of adopting a CMS to build a website, especially if it’s a new set of tools, is that it is not going to be supported. In today's race for better software, the idea that support will dry up for any piece of software or equipment is a very real one.

If you build a site using a CMS then the community for that CMS vanishes and the developers stop supporting it, you are going to hit a brick wall. At some point, you won’t be able to update the content management system or its plugins anymore.

A look at the [Drupal project's website](https://www.drupal.org/community) and a quick browse through its blog and community pages show that this is no issue. More groups are forming within its community; more developers are learning how to use the tools; more modules and themes are being created every day.

Drupal has been growing for 17 years, becoming one of the primary website building tools for developers. It shows no signs of slowing down yet.

### Structure Your Content With Taxonomies

If you are building a large site with a lot of content, like a blog or a community site, then the map of that site is very quickly going to grow into a real sprawl. It’s a lot of content to manage, but the right tool can make it a lot easier to do just that.

Drupal uses a fantastic [system of taxonomy](https://friendlymachine.net/posts/overview-drupal-taxonomy), which allows you to tag, categorise, and manage content with any definition you want to apply to them. For instance, many bloggers blog under different categories, and taxonomy allows them to separate the content, showing readers only what is most relevant to their interests.

### Allow Your Website to Grow

Being able to change the content of a site is different to being able to grow the website and to add entirely new functions and features well-beyond its launch. With Drupal, you can do both. Now, if you have a new idea for your site, you don’t have to start from scratch.

You can grow from a site with ten pages to a site with ten-thousand, constantly changing what kind of content and features you add without having to break the rest of it apart. The performance of the site and its security functions can scale with you. The website’s load time, how it handles traffic, and more can be managed from the Admin dashboard.

### Drupal Core Is Secure

There’s also a reason that sites as important as the homepage of the White House in the USA have been built with Drupal as well. Among open-source, free CMS tools, there are few that provide as reliable a set of security features. The work of Drupal’s security team has become one of the cornerstones of its great reputation.

Never does a concern go unreported and, when it is, few developers respond as quickly with a security update. The Drupal project team is one of the most dedicated and use an orderly, easy-to-use process to allow anyone to submit an issue with any site or module.

## How Does Drupal Compare?

The features above are all great reasons to consider using Drupal, but there are other CMS platforms to consider. Here, we’re going to look at how it stacks up against the other content management systems on the market.

W3Techs did a website technology audit, which shows the current content management system usage among website owners. Drupal's current user base is 4.1%, 

Joomla comes out in second place with 6.1% and WordPress takes the lead with 59.9%. As you can see the amount of Drupal websites in existence is small compared to WordPress, which grows in popularity year on year.

| Platform | Usage | Change since Apr 2018 | Market Share | Change since Apr 2018 |
| --- | --- | --- | --- | --- |
| WordPress | 30.8% | +0.4% | 59.9% | \-0.2% |
| Joomla | 3.1% | \- | 6.1% | \-0.1% |
| **Drupal** | **2.1%** | **\-0.1%** | **4.1%** | **\-0.2%** |
| Magento | 1.1% | \-0.1% | 2.2% | \-0.1% |
| Shopify | 1.1% | +0.1% | 2.1% | +0.1% |

Source: [W3Techs.com](https://w3techs.com/technologies/overview/content_management/all)

## Drupal vs. WordPress vs. Joomla

WordPress might very well be the most famous web building tool on the planet, with over 74 million websites built with it. They’re all using it for a reason.

Joomla has a community just as strong as Drupal's There are many similarities between the three. They are all free and open source. They are very search engine optimisation friendly, allowing for easy keyword entry. But what are the differences and which should you use?

The answer depends on what you want from your website. Over Drupal, WordPress has an ease-of-use built into it, with a much more understandable back end accessible even the most novice of users. Drupal requires a bit more knowledge to use from the start but can be updated easily with modules that allow for that same level of accessibility.

A more experienced developer can quickly put in place the tools for you to be able to edit, add, and manage content yourself. Joomla has even more options from the get-go than both tools, which can needlessly complicate navigation for a lot of first-time users.

Versatility is another matter entirely. WordPress has plenty of plugins, which allow for design that can be nearly as versatile as Drupal’s modules. However, plugins rely on their own developers to consistently update them. If they lose the support of the developer, it is like dead weight suddenly pulling your site down.

Modules also have many more issues regarding poorly written code and incompatibilities. The more you use on the site, the more risk you have of creating bugs that use all your resources, slow the site, and even break pages entirely. Modules allow Drupal a broader range of creativity without the hassle of plugins.

Security risks are not the only reason you have to worry about what you add to your site and how you scale it. Beyond plugins, WordPress is built primarily for adding content for sites like blogs. Adding a lot of content that is outside of that mold can make the site a lot slower.

## Conclusion

Every content management system has been built with a specific use case in mind but thanks to extensions, plugins and themes, content management systems can do more than what they were originally intended for. The main things you should consider are:

- **Security** \- is the platform secure? Before you start building your website on a framework you need to know that security has been thought of and applied correctly.  
    
- **Usability** - is the platform easy to use? You or your staff will have to use this system to update the website moving forwards, if the system is hard to use then its not fit for purpose.  
    
- **Scalability** - every business or website will grow and you should ensure that your content management system is able to scale along with your business.  
      
    If you can't extend the functionality or modify the website at a later date then it stunts future growth. You want to choose a framework that is modular in nature and allows you to customise it to your needs.  
    
- **Reliability** \- is the platform reliable? Content management systems will release software updates for various reasons and sometimes those updates could cause unforeseen problems. Have a look on Internet forums to see what issues people are facing.

Having used all three platforms, we choose to use WordPress for 95% of the website we build. It is very intuitive for non-tech savvy users and our customers find it easy to use and update. Choosing a development platform isn't easy and we would suggest you note down your current requirements and brainstorm future requirements as well.

By thinking of future progression, you can plan for it in the initial website build as it may affect how you approach the development of the website.

**What content management system do you use and why?** We'd love to hear about your experiences in the comments.
