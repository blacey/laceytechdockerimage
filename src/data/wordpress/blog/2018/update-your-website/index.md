---
title: "How Often Should You Update Your Website?"
date: "2018-08-08"
categories: 
  - "web-design"
  - "development"
tags: 
  - "web-design"
  - "web-development"
  - "website-updates"
  - "websites"
---

There is no specific time in terms of months or years that you should rely on as to when to update your website, but there are many factors that may influence your decision to do so.

It may seem like a lot of unnecessary expense and effort, keeping your website as up to date as possible offers many benefits, which we will explore in this article. Technology moves quicker than, "The Flash" these days so it is important to keep up to date.

There were many speed improvements added to PHP 7 but many website hosting providers are still using PHP 5.6, which is out-dated. Despite all this, it is possible to have a great website that offers a variety of relevant features for your customers.

## Why should you update your website?

Visitors may visit your website on a regular basis, this is especially true if you have a members only section to your website. Updating images for your key products and services is a good way to keep interest for return visitors.

Updating the content on your website to give detailed information to your visitors is another great way to keep prospect customers coming back to the website.

You should always focus on your users and not search engines - if your customers have a bad user experience when using your website it could cost you a potential pre-sales enquiry.

Google looks at how often your content has been updated when it does periodic scans of your website. If you do not update your website's content on a regular basis you may find you experience a drop in rankings.

## When should you update your website?

As a quick guide, think about the following questions - if you answer yes to any of them it may well be time to consider updating your website.

### Is it more than 3 years since you last updated your website?

As we said, technology moves fast, and 3 years old might, in fact, mean 3 years out of date. Your website should reflect your business aims, objectives and goals and these change over time so you should be updating your website regularly. The fonts, colours and branding may evolve over time and these changes need to be in sync with your website and online presence.

Does your website match your current branding?

Businesses grow over time, and your original website may not have grown with you. You may be offering a more diverse range of products for example, or you may be targeting a different audience. Your website needs to reflect your brand as closely as possible, and that includes the way it looks, the tone of your content and the products or services you offer.

Is your website friendly?

It’s 2018, and your competitors will all be offering shiny websites that shrink down to be easily accessible on tablets and smartphones. If your potential customers can’t navigate their way through your website when they are out and about, they won’t bother with it.

Over 60% of online searches are done on a mobile phone so if your [website is not mobile friendly](/blog/google-mobile-friendly-search-algorithm-update/) then this should be your top priority. Your customers don’t have the time to search through tiny script; they want clear, easy to navigate, responsive websites.

Is your Content Management System doing its job?

A lot of website owners will use some like of framework or content management system that allows them to log in and make updates to their website. Content Management Systems like WordPress, Drupal, Joomla and Magento have different features and depending on your business one system might be better suited to your needs.

Most businesses have a basic information website and over time they might want to add features like an online shop or a customer support portal and depending on the system you use it might make sense to change to a new system.

Quite often we find that customers stick with the original system the website was built on because "it's always worked" or "it's what we're all used to" and this can sometimes lead to compromises or custom coded solutions when it would be more appropriate for you to change platform instead.

Does your website provide features customers expect?

Your prospect customers often look for quick access to a blog, contact form, support forum or shopping functionality, all on one easy to use website.

They only want to log in once, and they want to be able to access everything they need, quickly and with ease. Just like a virtual shop front, your customers want to see everything under you offer clearly displayed on your website.

For example, Google uses single sign-on (SSO) where you log in once and you have access to Gmail, YouTube, Google Hangouts, Google AdWords and Google Analytics to name a few.

Do you have your social media profiles linked to your website?

This is a must for any website. Not only does it offer visitors to your site easy access to your social media sites, it also helps in terms of SEO. To get a great search engine ranking, you really need to be found on as many social media sites as are relevant to your business.

Are your contact details easy to find?

OK, so this may sound totally obvious, but you would be amazed how many companies make their customers jump through hoops just to find a phone number. Incorporate a contact link in the main menu, or be prepared to lose potential customers. Having contact details in a sidebar or at the top of the page is recommended to keep visitors on the website.

Does your website look out of date?

We appreciate that this is subjective, but seriously, if you are green with envy looking at other flashy, multi-level websites, whilst yours looks like an early version on AOL, then yes, it’s probably completely out of date. Of course, if you came across this blog by searching for “how often should I update my website”, there is a very good chance that yours is due for an upgrade as we speak!

Is your website slow to load on Desktop or Mobile devices?

Having a fast website won't directly result in better rankings, but it will mean that visitors are more likely to stay on your website for longer. Google is all about showing quality search results, and while page speed isn't a ranking factor, the more time people spend on your website is a positive engagement metric.

This shows search engines that visitors are interested in the content of your website and where Google is all about showing quality results you can hopefully see how a fast website can benefit you and your prospective customers.

In January 2017 we saw that mobile Internet browsing overtook that of Desktop and Tablets. Making sure your website loads fast on mobile will only seek to benefit your website for your customers, which is the important consideration here.

[![](images/StatCounter-comparison-ww-monthly-201401-201808-800x450-1.png)](http://gs.statcounter.com/platform-market-share/desktop-mobile-tablet/worldwide/#monthly-201401-201808)

StatCounter Chart showing desktop, mobile and tablet use from January 2014 - August 2018

### Security Considerations

You may have decided to change the underlying code or framework that powers your website and allows you to make changes. Some platforms enforce better security than others and its a valid consideration especially since the laws around GDPR have changed.

Any security breach where personal information is involved must be reported to the Information Commissioners Office within 72h of discovering the breach. Security breaches will happen from time to time and make sure you employ a range of security for your website will help you keep the trust of your customers.

Website security must be continually reviewed if it is to be successful at stopping hackers in their tracks. Our [website hosting](/services/website-hosting/) has automated blocking of suspicious activity like brute force attacks where a bot tries thousands of possible login username and password combinations. This is one aspect that could be a vital improvement to your website moving forward.

## The Benefits of Regular Updates

These are many and varied, but it is very beneficial to regular regularly update your website. Updated content, for example, is a great way to improve your SEO rankings. Google has a preference for new and original content on websites. This could be contained in your copy, or by adding a blog page.

The security of your site will also be increased with regular updates. Your website will have been sitting around on some dusty old server for a couple of years now probably, and it would not hurt to update passwords and security features to help prevent against hackers.
