---
title: "The REAL Cost of Bad SEO"
date: "2018-03-02"
categories: 
  - "seo"
tags: 
  - "bad-seo"
  - "seo-2"
---

This article discussed what bad website optimisation looks like and how this can affect your website. You can find out how to hire a good seo company, and what questions you should ask them.

As well as knowing what to look out for, you should also know how to spot a rogue SEO company. Ideally, you should take action to make sure that you don’t make these mistakes when organising search engine optimisation.

If you’re unsure of what you need to avoid, we detail the outdated SEO practices that should be avoided. Bad SEO (also known as Black Hat SEO) could damage your reputation and cost your business valuable time and money.

Search engine optimisation is all about making sure your website can be found by potential customers. If you fall victim to bad SEO practices, there are many things that can go wrong, and they all result in a potential loss of income for your business.

There are some ideas and concepts that you need to get your head around if you’re going to understand why SEO matters. You will learn about everything you should know in the guide below. As well as learning what bad SEO looks like and its effects, you can find out how to seek out a good SEO company.

## What Happens If You've Hired A Bad SEO Company?

When you make SEO mistakes, they will be spotted by search engine algorithms. Once they’re spotted, you will likely receive a notification in Google Search Console if you have this set up.

If you receive a manual penalty then you will have a short window to correct the issues. Black Hat SEO techniques that go against search engine best practices, will likely result in a drop in website rankings, which in turn will mean fewer website visits.

The higher you rank, the better it is because it’s this that results in more people heading to your website. So, any downgrade your website experiences **will have a negative result on your company as a whole**. It could mean your company misses out on customers who instead head elsewhere for what you offer.

Your website could even get removed from the search engine’s index. This is the thing that you really don’t want to happen. It could spell long-term disaster for your business because your website will be almost impossible to find, and that makes finding customers hard for your business too.

> "Google only loves you when everyone else loves you first"
> 
> Wendy Piersall

These are the direct consequences of bad SEO techniques, and they shouldn't be dismissed or underestimated. Ideally, you should take action to make sure that you don’t make these mistakes when organising your own website optimisation.

If you’re unsure of what bad SEO looks like and the things you need to avoid, the information below will help you spot this. It’s important to build your knowledge of bad SEO practices, so you can avoid them or correct the issues quickly.

## What Does Bad SEO Look Like?

### Duplicate Content

Content that is not new and unique is always bad for your website. It will impact your ranking, so it should be avoided at all times. It is sometimes tempting to re-post content to keep things flowing and ensure that your website does not appear inactive. Of course, inactivity is not great for SEO either, but the algorithms are smarter than you think.

They can spot duplicate content easily, and your website will be punished as a result. Not [indexing](https://blog.kissmetrics.com/get-google-to-index/) these duplicate pages is a good idea if you do have a legitimate reason for having duplicate content. That way, search engines won’t view it as a fresh index, and you won’t be punished for it.

When we do an [SEO Audit of your website](/services/search-engine-optimisation/website-seo-audit/), we look for duplicate content and list all the affected pages within the report. Duplicate content can be caused by copy and pasting content from other websites or by a technical issue with the website.

### Low-Quality Content

The quality of your content matters. It’s easy for search engines to spot content that has just been churned out for the sake of it, and it’s not something that you should let happen. Low-quality guest posts can be particularly damaging. It’s important to be careful when selecting guest posts.

If they are not up to the required standard, and they don’t really offer much to your website, they could actually do more harm than good. Make sure both the length and the overall quality of the website is up to scratch. If it’s not, your website rankings may be adversely affected, and that’s the last thing you need.

Sometimes, just by putting yourself in your audiences shoes can sometimes give you a fresh perspective on what you should be saying on each page. By thinking about who you're marketing to, you can tailor your marketing message to each type of customer.

### Paid Links

Buying or selling links is a violation of [Google’s Webmaster Guidelines](https://support.google.com/webmasters/answer/35769) and will result in a manual ranking penalty if you are caught (and you will be - eventually). Paid links might seem like a good idea at the time, but they’re not always the right way forward for your website. Natural back links are always preferable as it means your content is good enough that others want to share it with others.

Building back links takes a ton of time and hard work, but they are different to the kinds of links that get paid for. You should try to remember that when deciding whether or not to use them. It could be much better for your company as a whole if you avoid paid back links.

Search engines are getting smarter every day and with the help of [artificial intelligence (Rank Brain)](/blog/google-rankbrain-artificial-intelligence-search-queries/) they are able to work out if a link has been paid for. It is best to stay away from paid links and focus on producing content (blog posts, news, press releases etc) that are more likely to earn you links from authoritative sources within your industry.

Now for the question, I'm sure you're thinking of...

### Do Paid Links Work?

This is a question that has seen different opinions from both sides - those who engage in the practice of paid link buying and those who refuse to give it the light of day. From our experience it would appear that clients often have a mix of paid and earned links in their back link profile, some know about the paid links and some don't. This can be for a range of reasons and it often stems from rogue SEO agencies buying links and making out that they spent time on prospect outreach.

> "We discovered that 78 per cent of businesses who buy legitimate paid links consider them to be efficient backlink building opportunities. Similarly, 69 per cent of businesses using paid link buying considered their campaigns to be efficient, according to our poll."
> 
> Search Engine Journal - [Buying Backlinks for SEO](https://www.searchenginejournal.com/buying-backlinks-for-seo/207510/)

### Repeated Keywords

Keyword stuffing is a trap that many people fall into when they are trying to get SEO right. It might seem like a good idea to cram lots of keywords into your content. After all, keywords are good, right? Well, not exactly. As far as search engines are concerned, repeating keywords is a sign that you’re trying to manipulate the search engine algorithm.

That’s why search engines take it so seriously. They don’t want to reward sites that are so clearly trying to cut corners and gain an unfair advantage over the competition. Stuffing your content with too many of the same keywords will see your website penalised and you can kiss goodbye to those page 1 rankings.

### Ignoring Title Tags and Meta Descriptions

We see this time and time again, a company gets a website setup by an agency who only build websites and you end up with "<Page Name> - <Website Name>" as the default. Title tags need to be engaging to promote clicks from search engine results pages.

SEO title and meta descriptions should never be ignored or set to the default. When you don’t include a title or description then your website’s chances of getting on page 1 are slim. SEO title and description tags are HTML code that allows you to specify what appears in the search engine results list.

Without them, Google is not going to give you a decent ranking; it really is as simple as that. Ignore these two things, and it will be impossible for you to do well when it comes to SEO. So, don’t ignore them any longer. It might seem complicated, but once you know the basics, it’s really easy to get right.

> "People will come to your site because you have good compelling content. You need to hit it from all angles: [blog posts](/blog/), articles, graphs, data, infographics, interactive content - even short pictures when you Tweet"
> 
> Chris Bennett, Founder of 97th Floor

## How Do You Find a Good SEO Company?

Sometimes, the best way to get your website’s SEO right is to seek help from SEO consultants who _really_ know what they're doing. But with so many SEO companies out there, it is essential that you find a company with a [proven track record of success](/case-studies/).

### Look at Their SEO results and past work

First of all, you need to have a look at their track record. How much work have them done for other customers that are similar to you and your company? Have they got any direct evidence of their skills and abilities? It’s so much easier to trust a company when you can see that they have previously done the kind of work that you need to get done.

You will feel much more confident using their services once you have looked at their portfolio and judged it for yourself. You should never hire a company before taking a look at what their past work is like. It’s never advisable, and certainly not when it comes to SEO work.

### Ask Questions

If you have any questions that you want to ask the company representatives, make sure you ask them in advance. It’s never a good idea to rush into things without know where you stand. And if you ask these questions when you’ve already signed on the dotted line, it will be too late to change anything if there are any concerns that do arise from the answers you receive.

Many people worry that they are being too fussy or bothering companies too much. But if they want your custom, they should be more than willing to spend time answering the questions you have, so don’t hold back at all. It’s the least you deserve.

### Check Out Their Website

It can also be a great idea to look at how the company itself deals with SEO matters. You can find out a great deal by simply looking at their own website and seeing the steps they take. If they can’t even get the SEO basics right on their own website, it should tell you something.

If their SEO is good and they rank high in your area, then they could be the company for you. Of course, you shouldn't base your entire decision on this because there are other things to take into account. But it should definitely play a part in your decision when the time comes to make it.

### Look for Qualifications

If the people who are going to be working for you really know what they’re doing, they might have a Google Analytics Individual Qualification. It’s something worth looking out for when you’re choosing which company to work with on your SEO needs. It is a certification that shows they know what they’re talking about when it comes to managing websites, using analytics and getting SEO tactics right.

It’s this kind of evidence that can provide you with greater peace of mind going forward. Trusting other people with your SEO requirements is not always easy, so any reassurance you can get is positive. Our SEO consultants took the time to make sure they were certified and those exams have to be retaken every 18 months so you know their skills are up to date.

## How Do You Avoid Rogue SEO Companies?

As well as knowing what to look out for, you should also know how to spot a [rogue SEO company](https://searchenginewatch.com/sew/how-to/2171818/dead-giveaways-seo-artists-protect). There are too many of them out there, and you don’t want to get tripped up by them.

If they’re under-priced, it might be a sign that something's not quite right. In the UK you should expect to be paying around £500 - £1,000 a month on decent SEO services. You should get monthly reports showing the progress of your campaign and they should inform you of links they have gained through any outreach process.

Of course, everyone likes a cheap deal, but what use is that deal if you receive a terrible service out of it? Cheaper SEO consultants are often unqualified and some use out-of-date information about the industry.

Always dig deeper before choosing to use a service with a low price tag attached to it. If they promise number 1 rankings or things that seem far too ambitious and unrealistic, this is to be taken with a pinch of salt too. If it seems too good to be true, it probably is.

Some rogue companies might ask to retain copyrights to things like SEO and meta descriptions. This might not seem like a big deal, but it can be used against you. And it should definitely be avoided whenever possible.

If the company tells you tales of how they can improve your ranking by using illicit methods or by getting you preferential treatment because of connections to Google, you should run in the opposite directions. These kinds of things are never truthful.

## Is SEO a Worthwhile Investment?

Many business owners just view SEO as just another necessary evil. It’s not just one of those things that you have to spend money on, though. It’s much more than that. In reality, it’s about making sure that your business is available and easy to find for customers online.

These days, the main vessel through which people find new companies is Google. And if you’re not capable of getting a good ranking on Google, you’re going to be in trouble sooner or later. If your website doesn’t rank at all because of bad practices, the situation is even worse.

In truth, you should see paying for website optimisation as a vital investment that needs to be made. And by making it, you will make your business’s chances of success better in the [long-term](https://www.forbes.com/sites/ajagrawal/2017/08/27/how-voice-search-will-change-the-future-of-seo/#431388d17ca1).

SEO is not going to go away or fade into obscurity anytime soon. In fact, the opposite is happening. With each passing year, Optimising your website is important for growing your business.

So, if you want your business to thrive and increase its market share, getting your approach to SEO right is essential. For better or worse, not much can be achieved without it these days.

SEO isn’t something you should put on the back burner. In a world where a business’s online presence matters as much as anything else, good SEO is essential. Make mistakes, and your website could suffer badly.

**Have you had a bad experience with an SEO company? [Get in touch](/contact/) with us today and we can help you put right their mistakes.**
