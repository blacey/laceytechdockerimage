---
title: "Should You Buy Social Media Engagement?"
date: "2014-05-12"
categories: 
  - "social-media"
---

Not so long ago I was advising a client on the best ways to market their website after it was launched and the conversation quickly turned to social media.

We mentioned that investing time in social media would certainly raise awareness of their brand and allow them to communicate with potential customers. Then the client asked...

**"Why should I spend time on social media when I can buy a few hundred followers in a fraction of the time?"**

For a moment we were speechless and we really could't comprehend what we were hearing...

In our opinion buying followers and likes **is unethical, deceptive and it undermines consumer trust**. Long-lasting business relationships are built on trust and by purchasing fake followers you are essential lying to your customers, which could do more harm than good.

We appreciate people are too busy focusing on their business and rarely have any spare time to put into marketing. There are benefits that make this service tempting, but let's take a moment to think this through.

## Why You Should Avoid Buying Social Media Engagement

I've never bought followers or likes for my social media profiles so I'm not 100% sure of how the whole process works but essentially all your purchased followers will be low quality profiles that are fake or randomly generated by a script.

Every tweet or Facebook post you publish will appear on your social media account but very few people will view, share, like or favourite your content because they're fake profiles.

My customer thought that by having a lot of followers it would make out that their business was popular and that people should follow them. Whilst this might be true and it might work there is every chance that this will backfire. The best way for your company to get noticed on social media is for you to share content and start conversations with people.

Often the money you spend on fake followers will be wasted as most social networks will periodically delete fake and inactive accounts. More importantly when these fake accounts start disappearing, your real followers will soon realise you've bought social media engagements.

The practice of buying followers and likes may seem appealing to start with but you may lose the trust of your real social media followers and _**that**_ can hit any business hard.

## What Proof Do You Have That It Is Bad?

We don't personally have any proof but the team at AdSpresso **conducted an experiment** to see if buying followers and likes made a difference.

They set up 3 Facebook pages to test and compare the results of paying for Facebook Advertising and buying Facebook likes from 2 anonymous suppliers. Adspresso reported that:

"The two 'Buy Likes' pages performed incredibly poorly. Neither of them managed to reach 1 like per post on average".

It might seem like a good idea to make a start up business seem more popular but it won't do you any favours in the long run. The team at Adspresso clearly dedicated a good amount of time conducting their study and its well worth reading [their experiment's findings](http://adespresso.com/academy/blog/buy-facebook-likes/).

## Why Do Businesses Use Social Media?

Ultimately businesses utilise social media to improve their brand awareness, build customer relationships, look for new work and update existing clients about what is happening with their company. For this to be effective you need to talk and interact with **_real people_**.

When you buy likes, followers or YouTube views someone is running a program and getting fake profiles to follow you. These profiles are meaningless and any effort to interact with your 'new followers' is a complete waste of time.

## You Can't Cheat The System

I've always believed that **you shouldn't need to cheat the system to be successful**. I've seen websites penalised for trying to [manipulate the search engines](/blog/interflora-penalised-by-google-for-offering-free-flowers/) with dodgy tactics.

In most cases it took months for those websites to regain their old rankings in Google. I'm sure most people are aware that search engines change their algorithms to eliminate spammers. Do you not think Facebook, Twitter and other social networks don't do the same?

It might seem like a good idea to buy fake likes and followers for your business but it won't pay off in the long run. Instead, take 20-30 minutes out of your daily schedule and engage with people on social media.

Earning followers organically isn't easy and _it will take time_, but in the end you'll have real followers who will be interested in your business and what you have to say.

**Do you have any thoughts you'd like to share?** Feel free to leave your thoughts in the comments below!
