---
title: "Google Uses 'Always On SSL' As a Ranking Factor"
date: "2014-08-21"
categories: 
  - "seo"
tags: 
  - "google"
  - "ssl"
---

Now that website's have recovered from the [SSL Heartbleed bug](/blog/heartbleed-openssl-venerability/ "Heartbleed OpenSSL venerability"), Google have announced that they're going to use SSL as a ranking factor in the search results. To start with having SSL enabled will be considered as **a lightweight ranking factor** in Google's algorithm.

Having SSL enabled will help [improve the security of your website,](https://http://laceytech.local/pwa/blog/improve-security-for-wordpress-websites/) which is nice if you're using a Content Management System but I wouldn't jump on the SSL bandwagon just yet. There are a ton of on-page and off-page SEO factors that provide more value than switching to HTTPS/SSL.

For small businesses investing in the set up and ongoing costs of an SSL certificate could put a big dent into their yearly marketing budget with very little gain. Over time secure websites will hold more weight within the search results and your rankings _may_ see an increase in rankings but its hard to say for certain.

> "Security is a top priority for Google. We invest a lot in making sure that our services use industry-leading security, like strong HTTPS encryption by default. Beyond our own stuff, we're also working to make the Internet safer more broadly. A big part of that is making sure that websites people access from Google are secure."Source: [HTTPS as a ranking signal](http://googleonlinesecurity.blogspot.co.uk/2014/08/https-as-ranking-signal_6.html), Google Online Security Blog (6th August 2014)
> 
> Source: [HTTPS as a ranking signal](http://googleonlinesecurity.blogspot.co.uk/2014/08/https-as-ranking-signal_6.html), Google Online Security Blog (6th August 2014)

## What is SSL?

SSL (Secure Socket Layer) is used to establish an encrypted link between two computers/devices. Using a secure link (https) means that all data that is passed between the web server and the web browser is encrypted and private.

<iframe src="//www.youtube.com/embed/dsuVPxuU_hc" width="100%" height="380" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

SSL Certificates are small data files that digitally bind a cryptographic key to an organisation's details. When installed on a web server, it activates the padlock and the https protocol (over port 443) and allows secure connections from a web server to a browser.

## Should I be using SSL on my website?

Standard (Non-encrypted) websites use HTTP, which is insecure and can be subject to eavesdropping attacks where a third party can access information that is passed from your website to the web server (things like credit card and website login details).

SSL is typically required on [E-Commerce website](https://http://laceytech.local/pwa/blog/getting-started-with-ecommerce/) but people can enable SSL to add additional protection to website login forms like those found in WordPress, Joomla, Drupal and a whole host of content management systems.

SSL is optional for smaller or personal websites but with Google now using SSL as a ranking factor it makes sense to secure your website with SSL if you can afford to do so. The annual cost of protecting your website with SSL will be based on your individual needs.

### SSL Provides 3 Security Guarantees

- Server authentication allows users to have confidence that they are talking to the true application server. Without this there is no guarantee of confidentiality or integrity
- Data confidentiality means Internet eavesdroppers cannot understand the content of the communications between your web browser and the web server, because the data is encrypted using SSL
- Data integrity means that a network attacker cannot damage or alter the content of the communications between your web browser and the web server, because they are validated with a cryptographic message authentication code

## Frequently Asked Questions

### What type of SSL Certificate do I need for my website?

If you're setting up SSL on your website for the SEO benefit then you can use any because Google aren't interested in what type of SSL certificate you have at this stage. John Mueller of Google has stated that Google doesn't care what kind of SSL certificate you use but Google may change this in the future. You can see the video where John discusses this [here](https://www.youtube.com/watch?v=fKQULFm2BQA&feature=youtu.be&t=33m%20target=).

### What is a Certificate Authority (CA)?

A certificate authority or certification authority (commonly referred to as CA) is an organisation that is responsible for issuing digital certificates. The digital certificate is used to certify the ownership of a public key.

### What are public and private keys?

Public and Private keys are used for online cryptography (SSL) and its important you understand the differences between them.  
  
**Public Keys** are made available to everyone via a publicly accessible repository or directory.  
**Private Keys** are given to the owner of the SSL certificate and must remain confidential.  
  
Whatever is encrypted with a Public Key may only be decrypted by its corresponding Private Key and vice versa. For example, if I wanted to send sensitive information to Bob, I would encrypt the data using Bob's Public Key. This means that Bob's corresponding Private Key is the only key that can access the encrypted information.
