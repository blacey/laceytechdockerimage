---
title: "What Is The Heartbleed OpenSSL Venerability?"
date: "2014-04-10"
categories: 
  - "website-hosting"
tags: 
  - "hosting"
  - "openssl"
  - "vulnerability"
---

Heartbleed is a serious vulnerability in the popular OpenSSL cryptography software that is used to encrypt communication between a user's computer and a web server.

The Heartbleed venerability (CVE-2014-0160) could allow an attacker to steal information like usernames, passwords and cryptographic keys currently in use by the server. This information is normally protected using SSL/TLS encryption provided by the OpenSSL library but the Heartbleed vulnerability circumvents that protection. The OpenSSL library is available on Linux, Unix.

> "The vulnerable Heartbleed code committed at 22:59 on New Years Eve in 2011 has given the interwebs a long-delayed but truly vile hangover. Questions are already being asked about how it remained undetected for so long and whether the vulnerability has actually been abused in attacks." - The Register

![What is Heartbleed?](images/heartbleed.png)

Source: [http://xkcd.com/1353](http://xkcd.com/1353)

A fix was made available on Monday, 7th April 2014 and we can confirm that all servers were patched with the Heartbleed fix on Wednesday 9th April 2014. None of the hosting clients were affected by this venerability thanks to preventative action being taken quickly.

The Heartbleed bug allows anyone on the Internet to read the memory of the systems protected by vulnerable versions of the OpenSSL software. The bug exists in the OpenSSL 1.0.1 source code and stems from coding flaws in a fairly new feature known as the TLS Heartbeat Extension.

## How Can The Vulnerability Be Fixed?

Normally when a security vulnerability is discovered the first thing you should do is change your passwords, but in the case of heartbleed this could make the problem worse if the web server hasn't been updated with the fixed OpenSSL library. If you login to an insecure server and change your password it could reveal the old and new passwords to an attacker.

As mentioned above there is a patch that was released by openSSL to resolve the issue. It is advised that the following actions are performed to ensure the security of all affected servers.

- Patch affected systems to OpenSSL 1.0.1g
- Remove old SSH keys and regenerate
- Revoke old keypairs that were just superseded
- Changing all user passwords both on the server AND within software like WordPress and Joomla
- Invalidating all session keys and cookies
- Evaluating the actual content handled by the vulnerable servers that could have been leaked, and reacting accordingly.
- Evaluating any other information that could have been revealed, like memory addresses and security measures

### Advice When Changing Passwords

When changing your passwords **do not** use the same password. Make sure your password is complex and at least 10 characters long. Be sure to use a combination of letters (uppercase + lowercase), numbers and special characters (-, +, &, ^, \*, %, $, !, @, #) - if you can't think of a secure password you can use a service like [SafePasswd](http://www.safepasswd.com/) to generate one for you.

Where possible I would urge you to use 2-step authentication. 2-Step authentication helps to keep bad guys out, even if they know your password. For 2-step authentication to work, you need to install an application on your smartphone often called an "Authenticator".

You then need to setup your authenticator with the services that allow this security method. Once setup is complete, the authenticator will randomly generate a unique code every 20-30 seconds - this code must be entered along with your password so you can sign-in to the relevant service.

Google, WordPress, World of Warcraft, Banks, Government and a handful of other organisations already use this method to improve password security.
