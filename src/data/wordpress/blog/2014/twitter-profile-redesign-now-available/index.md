---
title: "Twitter's Profile Redesign Now Available To Activate"
date: "2014-04-24"
categories: 
  - "social-media"
tags: 
  - "social"
  - "twitter"
---

On the 8th April, Twitter announced that they are making their [new profile redesign](https://blog.twitter.com/2014/coming-soon-a-whole-new-you-in-your-twitter-profile) available to everyone on Twitter. The new profile has a larger text, images and a customize profile that is designed to be more of user-friendly.

## So what's changed?

### Popular Tweets

The new profile shows popular tweets in a larger font. Users can also pin a particular tweet at the top of their home feed, can also filter feeds so that they only show tweets with images or video.

### Larger profile and cover images

The new profile design includes a larger display picture and wider cover image, which is similar to Facebook. Your profile picture is 1500px wide, which is huge!

If your image is smaller than 1500px, Twitter will automatically scale your image. This may make the image appear blurred on larger screens - so be sure to check the changes!

### Emphasised Popular Tweets

The Twitter redesign highlights popular tweets in your timeline by increasing the font size. Tweets that have received good user engagement (replies, re-tweets and favourites) will be automatically marked up as a popular tweet.

The aim is to make your popular content easy to read when scrolling through your timeline.

Most specific change is Twitter's profiles is the "Following list". Now it will not show as list it will show up as a collage of the accounts a user follows, with mini version of their profile images, cover images and basic info.

Before the 23rd April the updated profiles were only available to a hand-full of celebrity profiles but its now available to every account to activate.

So what are you waiting for? Activate your new profile and enjoy the redesign!
