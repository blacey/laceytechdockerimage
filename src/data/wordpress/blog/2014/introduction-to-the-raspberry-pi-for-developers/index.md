---
title: "Using The Raspberry Pi For Web Development"
date: "2014-04-08"
categories: 
  - "development"
tags: 
  - "raspberry-pi"
---

Last week Raspberry Pi launched a fantastic [new website](http://www.raspberrypi.org/ "Raspberry Pi") that really helps showcase their innovative and thought provoking product.

The new design seems to be based around the strong use of typography, icons and varied colour schemes to logically separate information on the website.

Not only does the use of different colours help in separating content it also helps them appeal to their target audience (young aspiring developers).

You'll see I used a similar technique when designing the [DeLaBaie Skincare](/work/delabaie-skincare/ "DeLaBaie Skincare") website - colour was used to separate the different product ranges offered by the client, which helped reinforce the overall brand.

The icons/sketch like graphics are suitable for all ages but would appeal more to the younger generation. They are simple, effective and have been used really well throughout the design. An example of this can be seen below.

![Icons used in the Raspberry Pi redesign](images/raspberry-pi-iconography.jpg)

I particularly like the font they chose to use for the content, its stylish, easy to read and it makes reading lengthy articles easier, which is especially important for lengthy tutorials and documentation.

## So...What is the Raspberry Pi?

In 2006 when Eben Upton, Rob Mullins, Jack Lang and Alan Mycroft saw that there had been a steady decline in the number of students applying for computer science at A-Level and together they came up with an idea to help get more people into computing.

And so the Raspberry Pi was born - a small (credit card sized) multi-purpose computer that allowed people of all ages to experiment and learn more about computing.

The Raspberry Pi gives people of all ages the ability to 'tinker' and freely experiment without the worry of mistakes costing them a lot of money.

With a modest price tag of just over £35 (for the pi only - no case) this little device is an excellent learning tool.

### What are the Raspberry Pi Specs?

At time of writing this the Raspberry Pi is available in two models:

**Model A** has 256MB RAM, one USB port and no Ethernet (network connection)

**Model B** has 512MB RAM, 2 USB ports and an Ethernet port.

The graphics card provides Open GL ES 2.0, hardware-accelerated OpenVG, and 1080p H.264 high-profile encode and decode. The Raspberry Pi's graphic card capabilities are roughly equivalent to the original Xbox console in terms of performance.

There are composite and HDMI out ports available on the board, so you can connect it up to an analogue TV through the composite or through a composite to scart connector. You can connect it to a digital TV or to a DVI monitor using a passive HDMI->DVI cable for the DVI. There is no VGA support, but adaptors are readily available.

## Website Development on The Raspberry Pi

Below are a list of some of articles I've found that give a good introduction to setting up your Raspberry Pi for [Website Development](/services/website-development/ "Website Development").

**Setup your Raspberry Pi for Ruby Development**  
This great article by Ray Hightower explains how to setup a Ruby development environment on your Raspberry Pi for those of you who are interested in Ruby on Rails. [Read Article](http://rayhightower.com/blog/2012/12/03/ruby-on-raspberry-pi/).

**Setup your Raspberry Pi for PHP Development**  
This well explained article by Gary Newell of Daily Linux User explains how to setup lighttpd, PHP and MySQL on your Raspberry Pi. [Read Article](http://www.everydaylinuxuser.com/2013/06/setting-up-personal-web-server-on.html).

If you want to hone your skills as a front-end developer then Coder could be right for you. Coder turns your Raspberry Pi into a mini [web server](https://http://laceytech.local/pwa/blog/monitor-and-secure-ubuntu-web-server/) that allows you to create web content using HTML, CSS and JavaScript via a browser-based IDE.

<iframe src="//www.youtube.com/embed/wH24YwdayFg" width="100%" height="350" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### Raspberry Pi Projects

**Raspberry Pi Media Server**  
The first article comes from Nick Peers of Tech Radar. This well explained article caught my eye as a potential contender for the AppleTV for streaming media to your TV. [Read Article](http://www.techradar.com/news/digital-home/media-servers/how-to-build-the-best-raspberry-pi-media-server-1163133)

**Use the Raspberry Pi as an Ad-Blocking Access Point**  
This article is written by Justin Cooper over at AdaFruit and is a great example of how the Raspberry Pi can be used to block Adverts from websites. Admittedly buying a Raspberry Pi for the sole purpose of blocking ads seems a bit overkill, but with the use of Squid Proxy and Shorewall you could turn your Raspberry Pi into an AD Blocking, web page caching firewall device! [Read Article](http://learn.adafruit.com/raspberry-pi-as-an-ad-blocking-access-point)

**Buy 64 Raspberry Pi's and create your own super computer!**  
It may sound crazy ... but Professor Cox from Southampton University has proven to the world that you can build a Raspberry Pi super computer on a limited budget with a bit of computing know-how and with the help of Lego (Kudos for the use of Lego Professor!). [Read Article](http://www.southampton.ac.uk/mediacentre/features/raspberry_pi_supercomputer.shtml)

**If you've come across some great projects that aren't mentioned here please share in the comments below.**
