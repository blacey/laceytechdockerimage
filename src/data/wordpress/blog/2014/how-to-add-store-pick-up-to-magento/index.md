---
title: "How To Add Store Pick Up Delivery in Magento"
date: "2014-01-15"
categories: 
  - "ecommerce-development"
---

Recently we were asked how to add a store pick up delivery option in Magento so we thought we would write a tutorial.

We've seen various paid Magento extensions that provide this functionality. This guide will show you how to set up store pick up for free but this is an advanced post.

This tutorial will allow you to add store pick up to the Magento checkout page. If you have multiple stores then this tutorial isn't for you.

**Step 1:** Login to your Magento store by visiting www.yourdomain.com/admin and logging in using your username and password.

**Step 2:** In the main menu, hover over 'System' and then click 'Configuration'.

**Step 3:** On the left hand side click, 'Shipping Methods'.

**Step 4: Setup the Shipping Rate**

![](images/magento-fixed-rate-shipping-method.jpg)

In the 'Flat Rate' panel make sure the 'Enabled' drop-down is set to Yes.

In the 'Title' box type in your pick-up option eg: 'Store Pick Up (Farnborough)'. Whatever you put in this text box will be shown to your customers during checkout.

Keep the 'Method Name' drop-down set to 'Fixed' (Fixed Rate shipping).

Make sure the 'Type' drop-down is set to, 'Per Order'.

In the 'Price' text box enter a price of '0.00'.

You can set a handling fee using the boxed provided if this is required, otherwise leave these blank.

You can customise the error message but the default is fine in most cases.

Make sure the, 'Ship to Applicable Countries' drop-down is set to, 'Specific Countries'.

In the 'Ship to Specific Countries' box click on your desired country - in my case this would be, 'United Kingdom'.

Then lastly make sure the, 'Show method if not applicable' is set to 'No' otherwise this delivery option will be shown for UK and International visitors.

**Step 5:** Save your Magento Config by clicking the save button.

**Step 6:** Place a test order to see the new delivery option in place.

* * *

**Magento Checkout Before:**

![Magento Checkout (Before)](images/magento-checkout-before.jpg)

  
**Magento Checkout After:**

![Magento Checkout (After)](images/magento-checkout-after.jpg)

We have also written an article on how to [track checkout abandonment rates using Magento and Google Analytics](/blog/tracking-checkout-abandonment-google-analytics/). This allows you to see where visitors are leaving the checkout process so you can improve conversion rates.
