---
title: "Facebook Introduces Verified Pages"
date: "2013-05-31"
categories: 
  - "social-media"
---

Yesterday (30th May 2013) Facebook announced that it was introducing **Verified Pages** for businesses and public figures. This change was introduced to help Facebook users distinguish between official and fake pages on their social network.

## How do I get my Facebook page verified?

If your Page's category is **Local Businesses** or **Companies & Organisations**, your Page may be eligible for a grey [verification badge](https://www.facebook.com/help/196050490547892). If you're an [admin](https://www.facebook.com/help/289207354498410), your Page has a profile picture and cover photo, and is eligible, you'll see this option in your Page's **Settings**.

To verify your Page, you can use your business' publicly listed phone number or a business document (ex: phone bill). Facebook will only use this information to verify your Page.

Follow these steps to verify your Facebook page:

1. Click **Settings** at the top of your Page
2. From **General**, click **Page Verification**
3. Click **Verify this Page**, then click **Get Started**
4. Enter a publicly listed phone number for your business, your country and language
5. Click **Call Me Now** to allow Facebook to call you with a verification code
6. Enter the 4-digit verification code and click **Continue**

If you prefer to verify your Page with a business document, follow the steps above and click **Verify this Page with documents instead** at the bottom left of the window that appears, then upload a picture of an official document showing your business name and address.

Once Facebook has received your verification code or business document, they'll review your information to confirm that it matches public records and send you a notification or email about your verification status within a few days.

**Note:** If your Page represents a celebrity, public figure, sports team, media or entertainment, you may be eligible for a blue verification badge.

## How can you tell if a Facebook page is verified?

If your Facebook page is verified you will see a small blue tick in various places around Facebook so its clear that your page is authentic.

The blue tick is more noticeable to the right of the page name but you can also see it in status updates when people likes the page, when hovering over the page's name in the news feed and in Facebook adverts.

![Madonna's verified Facebook Page](images/madonna-verified-facebook-page.jpg)

Madonna's Facebook has been verified and because she is a public figure, she has a blue badge next to her page name.

Newly verified pages will make owners aware via Facebook notification. According to Facebook, advertising spend levels will have no impact on the verification status.

## Why are verified pages important?

It would appear that Facebook is trying to position themselves as a real-time content broadcasting website, to compete with Twitter and other social media website. It is clear that Twitter is currently the dominating website for small real-time bursts of information.

Its very easy to setup fake pages on Facebook and there seems to be no approval process when creating a Facebook page to ensure its authenticity.

With verified pages it helps indicate to Facebook users that the business page they're viewing is official and that is can be trusted. It will also help ensure that status updates and news reports are coming from the official source and not a fake page that's trying to get publicity.

### Are Facebook Focusing on The Wrong Thing?

Verified pages are all well and good but its too easy for people to create new pages on Facebook. I feel more should be done to help restrict people from creating pages or at least have some kind of approval process put in place to help stop fake or hateful pages being created in the first place.

This update seems to be addressing the symptom and not the cause, but I do appreciate that having a manual approval process in place could be inconvenient.

I've seen a number of hateful pages being created on Facebook, such as the widely publicised, "Cancer is funny because people die", which has recently been taken down.

What's your thoughts on **Facebook verified pages**? Feel free to leave your comments below.
