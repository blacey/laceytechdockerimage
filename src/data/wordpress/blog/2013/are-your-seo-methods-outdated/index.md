---
title: "Are Your Search Optimisation Methods Outdated?"
date: "2013-06-05"
categories: 
  - "seo"
tags: 
  - "seo-2"
  - "seo-techniques"
---

Surely you want your business to get noticed on the internet. Since search engines are go-to tools for most users whenever they want to find info or services and products, wouldn't it be great if your company appeared in search results?

All of this is possible through search engine optimisation (SEO); but what if you make it to the first page and your website traffic doesn't increase? What could be the problem?

Well, you may be following advice that is outdated, and are no longer effective at bringing in traffic to your website.

## Submitting Your Website To Article Directories

Sending content to an article directory is one of the fastest ways to get links back to your site. The more links you get, the better your site's chances are of achieving good placement on search engines.

However, if you build a lot of links in a short amount of time, search engines get suspicious, since it would normally take time to acquire them naturally.

So instead of relying on quick links to get you to the top, try to build relationships with other website owners within your niche. Visit various industry-related blogs, engage with the owners, and grab hold of guest-posting opportunities. You'll find that exposure to their audience may help you secure quality links from authoritative sites.

## Writing Low Quality (Thin Content)

Every article you put out should contain great substance. If you promised to give answers to a solution, do that. If you meant to give useful tips, make sure they're really helpful for your readers.

Above all things, remember to write for humans and not for search engines. Sure, using keywords in your content will help you get indexed, but did your post satisfy the need of readers?

Always keep the satisfaction of your target audience in mind the next time you write a post. Think about search engines later and focus on creating content with useful information that flows naturally.

## Ignoring Social Media In Your Marketing

You might think that Facebook, Google+, and Twitter are only a playground for today's young. However, many businesses are now using these platforms for marketing purposes. This is a sign that you should be doing the same thing as well.

But if you already have a presence on several social media sites, what are you doing to draw the attention of potential customers? Are you using Twitter hashtags to reach a wider audience? Are you interacting with your followers? Are you sharing interesting and relevant content?

Simply being active on any top social networking sites can help drive traffic back to your site, keeps your followers engaged, and builds trust.

## Participating In Link Exchanges

Would you swap a link with another site knowing it offers no value just for the sake of earning a link? What if you do exchange links with plenty of websites, wouldn't this tell search engines you're doing something so horribly wrong?

You should always keep in mind that links on your site should make sense. They should give your visitors access to more related resources to make them understand the material better.

If you read a post you think your readers will find value, don't hesitate to link to it. You never know, maybe the person who owns the site you linked to might just return the favour.

## Only Thinking About Website Rankings

Well, the aim of optimisation is to get a really good placement in search engine results, isn't it? But the thing is that getting to the top cannot be rushed.

A lot of time and effort must be poured into creating content that readers will love. It's trial and error, whereby you determine exactly what you should provide for your target audience to keep them coming back for more.

Good SEO techniques help you achieve a decent position in the search results. With search engines looking to give searchers more quality results, take this as a sign to steer clear of old and dated methods.

**Are you guilty of any of these methods?** Do you know of any more outdated ones? Please don't hesitate to share these through the comments.
