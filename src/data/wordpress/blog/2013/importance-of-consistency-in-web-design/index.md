---
title: "Why Is Consistency Important in Web Design?"
date: "2013-10-23"
categories: 
  - "web-design"
tags: 
  - "design-consistency"
  - "website-design"
---

Effective web design relies on a number of factors. One of the most important of these factors is consistency. You can achieve consistency through a variety of methods from correct use of tabs to good use of fonts.

This article looks into some of the reasons why it’s important to have consistency - as well as some cases where it may even benefit to be inconsistent.

During a redesign project we had the client ask me why consistency was important when [designing a website](/services/website-design/) so we thought we would share our thoughts on the topic.

We've been designing and developing websites for around 10 years now and with anything we always adhere to the iron-clad rule that consistency is crucial.

You may think that consistency leads to a boring website design or a lack of imagination, but this cannot be further from the truth. A **consistent design** will mean that common user interface elements (Company Logo, Site navigation, page content etc) appear in the same place throughout the website, so when visitors navigate through your website they know where common elements are likely to be found on the page.

Effective [web design](/services/website-design/) relies on a number of factors. One of the most important of these factors is consistency. You can achieve consistency through a variety of methods from correct use of tabs to good use of fonts.

This article looks into some of the reasons why it is important to have consistency - as well as some cases where it may even benefit you to be inconsistent.

## Consistency in Web Design Layouts

Layout of the website should be consistent enough as it shows the overall outlook of the website. The website layout refers to the basic framework and structure of the website - It determines where the text content, navigation, images and other important elements are placed on the website.

If these common website elements are in the same place on every page it means your visitors spend less time trying to use your website and more time engaging with your content.

There's also a time/cost saving benefit to a consistent design. If your website uses a consistent layout it means that your website developer only needs to create a small number of re-usable templates for your pages. Not only does this keep your website simple and easy to use, it also cuts down on the overall development time of your website, which can save you money.

Corporate websites should have a consistent colour scheme throughout the website to reinforce their brand identity. If you look at this website for example - our logo always appears on a blue background and that is reflected not only on the website but all social media profiles.

### What makes a website consistent?

- Does your website use the same colours throughout the website?
- Is there consistent vertical and horizontal spacing between elements in the layout?
- Are website headings consistent throughout the site?
- Do you consistently show navigation menus in the same place throughout the website?
- Are text links the same colour throughout the website?
- Are the website icons of the same family?
- Do form elements (text inputs, select lists, submit buttons etc) look the same throughout the website?

This is explained further in the video below.

https://www.youtube.com/watch?v=Dc3\_KBBilvA

_This video indicates ways to improve the visual design of a website by using elements to keep the website consistent._

## A sense of identity

Lots of different texts and fonts or multiple pages with differing layouts can cause a website to lose its sense of identity. This in turn can make it less memorable. If somebody is visiting lots of different plumbing company sites in their area in order to choose a plumber, they’re less likely to remember the site that doesn’t seem to have a set theme.

Using the same fonts throughout, sticking to the same writing style and keeping to a colour code can help to create a sense of identity by creating a theme.

People react more positively to a sense of harmony. In the case of a company website, they may be more willing to trust the company as an organised business if their website feels equally organised. A cluttered mess of ideas might lead them to believe that the business is equally disorderly, prompting them to look for services elsewhere.

On the subject of company websites, you should also be careful that a website’s user interface ties in with their overall brand. The company may already have a logo and a colour pattern that is used in other branding such as on signage, leaflets, branded t-shirts and vehicle branding.

Using a different logo or a colour pattern to the rest of their branding could create a sense of discord within the brand, which could lead clientele to question whether that website belongs to that same company.

## Ease of Navigation

Consistency can help to make navigation easier. If the navigation changes from page to page, people are more likely to get lost. Most sites will have a clearly divided navigation bar that can be accessed from any page on the website. This is usually at the top of the website or at the right hand side.

Having a search bar that’s accessible on every page can also help. It is possible to get creative with navigation as some websites have done. However, even these websites have a sense of consistency either in the colours or fonts that balances out these experimentation's.

Remember that navigation is likely to differ from [computer to mobile use](https://www.thoughtco.com/write-web-pages-for-mobile-devices-3469097). You should aim for your website to be functional on both mediums but still be similar enough in style in each case. If your mobile site is drastically different from your computer site, users may get confused when switching between the two.

This also relates back to identity – you do not want a mobile user thinking they have ended up on a different company’s site, having previously used your website on their PC.

## Good communication of information

When it comes to easily communicating information with your user, consistency is also important. If information is randomly plotted around a website, users may not find what they’re looking for.

You should also take care with use of bold writing or italics. This will immediately draw people to certain types of information and this information must all be of the same level of importance in order for the web page to feel consistent.

[Font sizes](https://www.imarc.com/blog/best-font-size-for-any-device) are particularly useful for highlighting certain information. Larger headers will help to divide sections of text. However, beware of using too many different font sizes - having headers and sub-headers and sub-sub-headers could start to confuse the reader.

There are some web design trends that have stuck when it comes to displaying information and you should be aware of deviating from these trends. For example, websites that allow users to log-in generally have a login box in the top right hand corner – you do not want to be hiding this near the bottom.

## Are There Times You Can Be Inconsistent?

Breaking the rules of consistency can have its benefits – so long as you don’t do it too often. If something is jarring, people's attention will be drawn to. Many web designers have started using this to highlight important information that may be different from other categories on the site. For example, a different colour may be used to highlight a ‘subscribe here’ function or contact details.

It's also often worth embracing the potential of [white space](http://www.creativebloq.com/features/10-great-uses-of-negative-space-in-web-design). In most cases, there should be equal spacing between tabs and paragraphs as this makes a website easier to navigate. However white space can sometimes be used just as effectively to highlight information.

This could include having a greater space between two tabs on a navigation bar, perhaps to separate general categories and a contact page. Or alternatively, you could be writing a blog post and decide to put a larger paragraph break before author details or a bibliography at the end.

Opposed to using a different font or a different colour, irregular use of white space may be less jarring on a conscious level. On a subconscious level, it’s likely to still attract a user’s attention however.

The trick is to not go overboard. Most websites will use only a single inconsistency - two at most. Too many inconsistencies will make each one stand out less and make it seem less purposeful and more chaotic.

![](images/Webp.net-resizeimage-10.jpg)

## Frequently Asked Questions

### How can I keep my website consistent?

Using a CSS framework like [Twitter Bootstrap](/blog/why-use-twitter-bootstrap-for-mobile-website-development/ "Why use Twitter Bootstrap for mobile website development?") allows you to keep a consistent layout throughout your website. If you use WordPress to manage your website you can easily integrate Bootstrap into your theme so pages, categories and posts follow a consistent layout.

### Is there any recommended reading material for website design?

**[Learning Web Design: A Beginner's Guide](http://www.amazon.co.uk/gp/product/1449319270/ref=as_li_tl?ie=UTF8&camp=1634&creative=19450&creativeASIN=1449319270&linkCode=as2&tag=lacetechsolu-21)**  
Do you want to build web pages, but have no previous experience? This friendly guide is the perfect place to start. You will begin at square one, learning how the Web and web pages work, and then steadily build from there. By the end of the book, you will have the skills to create a simple site with multi-column pages that adapt for mobile devices.

**[HTML and CSS: Design and Build Websites](http://www.amazon.co.uk/gp/product/1118871642/ref=as_li_tl?ie=UTF8&camp=1634&creative=19450&creativeASIN=1118871642&linkCode=as2&tag=lacetechsolu-21)**  
Every day, more and more people want to learn some HTML and CSS. Many books teaching HTML and CSS are dry and only written for those who want to become programmers, which is why this book takes an entirely new approach.

**[Web Designer's Idea Book, Volume 4](http://www.amazon.co.uk/gp/product/1440333157/ref=as_li_tl?ie=UTF8&camp=1634&creative=19450&creativeASIN=1440333157&linkCode=as2&tag=lacetechsolu-21)**  
Looking for inspiration for your latest web design project? Patrick McNeil, author of the popular Web Designer's Idea Book series, is back with more than 650 examples of consistent and unique website designs. The Web Designer's Idea Book is overflowing with visual inspiration.

**[Responsive Web Design with Adobe Photoshop](http://www.amazon.co.uk/gp/product/0134035631/ref=as_li_tl?ie=UTF8&camp=1634&creative=19450&creativeASIN=0134035631&linkCode=as2&tag=lacetechsolu-21)**  
Our multi-device world has shown us that this approach to web design, including full-page comps done in Photoshop, can be increasingly problematic. Until now, books on designing responsive Websites have been very code-centric, and visual creativity seems to take a back seat. This book is aimed at the visual Web designer who's accustomed to working in Adobe Photoshop.

## Are there examples of consistency for print media?

Books are a perfect example of consistency - if you were to open a book and find the table of contents in the middle and the index at the front, you would be confused. You would not know how to navigate the book because the design breaks the rules that you have learned. The same process works on the Web.

Your visitors have learned certain rules about navigating different types of websites (Business websites, E-Commerce websites, newspaper websites etc) and while it might be tempting to break the mold and design a cutting-edge site that breaks all the rules but there is every possibility that your website design will lost you site visitors.

These are just a few examples of what you should look out for when checking your website design is consistent. If you're website looks dated and needs a bit of a makeover, please feel free to [contact me](/contact) for a free no-obligation quote.
