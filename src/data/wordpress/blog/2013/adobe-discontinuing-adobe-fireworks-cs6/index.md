---
title: "Adobe Stops Development of Adobe Fireworks"
date: "2013-05-14"
categories: 
  - "web-design"
tags: 
  - "adobe"
  - "web-design"
---

Earlier this week I read an article on the Adobe Blog concerning the [future of Adobe Fireworks, and sadly Adobe have decided to discontinue the development of Adobe Fireworks.](http://blogs.adobe.com/fireworks/2013/05/the-future-of-adobe-fireworks.html)

The last version of this great design tool will be CS6, and Adobe have said that they will continue to sell Fireworks CS6 and make it available as part of their Creative Cloud service. Adobe went on to say that they will provide security updates as necessary and _may_ provide bug fixes.

Its unfortunate that they have decided to discontinue development of Adobe Fireworks, but in all honesty we've not seen many new features being added to this application in previous creative suite updates.

## Why Do We Like Adobe Fireworks?

**More accuracy than Adobe Photoshop** - In the Adobe Fireworks preferences pane we love being able to specify an X and Y coordinate for an element to aid in positioning it within a design and to my knowledge Photoshop doesn't have this feature.

**We love the interface** - Fireworks has an uncluttered, easy to use interface that I've loved since the first time we used Fireworks. In Photoshop common tasks are sometimes less obvious and takes longer than in Fireworks.

It would seem that Adobe are discontinuing Fireworks so they can focus on a smaller set of applications. It comes down to quality not quantity, and with Adobe having nearly 10 products in their creative suite we think its good that they are focusing on the quality of future updates.

The death of Fireworks CS6 will force us to use the other applications in the creative suite and stop using Fireworks for our design work. All good things must come to an end eventually.

What are your thoughts on Adobe's decision to discontinue development of Adobe Fireworks? **Feel free to leave your comments below.**
