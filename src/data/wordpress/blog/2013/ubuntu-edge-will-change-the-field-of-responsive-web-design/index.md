---
title: "Will Ubuntu's New Phone Change Responsive Web Design?"
date: "2013-08-23"
categories: 
  - "web-design"
tags: 
  - "ubuntu"
  - "web-design"
---

Linux often the operating system of choice when it comes to setting up a [website hosting](/services/website-hosting/ "Website Hosting") environment, but now we could see Linux stepping into (and possibly dominating) the smartphone marketplace. Ubuntu (a distribution of Linux) tried to fund an innovative and ground-breaking smartphone called, "Ubuntu Edge".

This meant that if the Ubuntu Edge concept failed to reach the $32 million target then there would be no Ubuntu Edge smartphone. Sadly they didn't reach their target but Canonical have not been put off. The Ubuntu smartphone is still "full steam ahead" - so we might see a Ubuntu Edge device in production within the next few years.

## Ubuntu Edge Smartphone Features

- The Ubuntu Edge smartphone would be capable of dual-booting between Ubuntu mobile OS and Android, while also allowing you to convert the phone into a full desktop PC
- The initial specs for the phone include a fast quad-core processor, 4GB RAM, 128GB internal storage and a 4.5 inch screen (720 x 1,280 resolution)
- Dual LTE antennas for full-speed 4G in Europe and the USA
- The screen would be made ofÂ Sapphire Glass offering improved screen protection to that of a standard smartphone

<iframe src="https://www.youtube.com/embed/eQLe3iIMN7k" width="770px" height="400px" frameborder="0"></iframe>

## How Does This Impact Website Development?

[Responsive website development](/services/website-development/ "Website Development") gives you the flexibility to change how your website looks depending on the device your visitors use to browse your site. This is done by looking at the screen resolution of the device and adjusting the layout of your website to fit the available space.

The Ubuntu Edge smartphone specs outlined the device's screen resolution to be 1280 x 720 on a 4.5 inch screen, which would redefine the boundaries of responsive web development. Suddenly we would have a smartphone using screen resolutions typically found in tablet and netbook devices.

The fact that Ubuntu Edge would give you both a smartphone AND a desktop computer in one device means that website owners cannot afford to dismiss responsive web design so easily. The aim of your company website is to ultimately get business leads/enquiry's whilst informing your visitors about your services and product range.

If you don't take every opportunity to make your visitors web browsing experience simple and easy you could lose out on a lot of potential business.

## How Will This Affect Website Developers?

Now that smartphones are getting more advanced and come with higher resolution screens it's really going to change how web developers code websites for mobile and tablet devices.

Typically mobile devices have had reasonably low resolution screens so its been easy to change the layout of a website to fit a mobile device.

Now that smartphones are becoming more advanced its going to be harder to define what device is a phone and what device is a tablet.

## How Can Websites Adapt For Phones and Tablets?

So far CSS media queries combined with a few JavaScript/jQuery libraries have been the best method to ensure your website responds and works on a range of tablet and mobile devices.

One of the issues with media queries is browser support as some web browsers don't support them (IE 7 and 8 for example), which is why JavaScript libraries are used to help provide support for older browsers.

There is always the fallback of checking the device's user agent but that's not as clean cut as reliable as people would have you believe. For example if you're browsing a website using an iPhone your user agent will show your using an iPhone but that same user agent is present when browsing a website using the iPad.

Responsive websites are still a fairly new concept and they certainly make it easier to browse websites on handheld devices, but we still have a long way to go before we arrive at a well-rounded solution.

**How do you think Ubuntu Edge will change how we use and build websites?** Feel free to leave your comments below.
