---
title: "Track Checkout Abandonment In Magento With Google Analytics"
date: "2013-01-09"
categories: 
  - "ecommerce-development"
tags: 
  - "google-analytics"
  - "magento"
  - "magento-checkout"
---

If Website analytics is setup correctly, they can give businesses all the information they need to make important website improvements to increase their e-commerce sales. In this article I'll show you how to successfully track **Magento shopping cart abandonment** using Google Analytics for Magento Community Edition.

Magento can track cart abandonment out the box but the information it provides is a limited and isn't very comprehensive. Using Google Analytics we will be able to see exactly when a customer abandons the checkout process.

**Please note:** This one works only with Magento's onepage checkout. I recommend you make a backup of your Magento website prior to following these instructions.

**Step 1:** Firstly we need to enable Google Analytics in Magento. To do this login to your admin panel, hover over the "System" menu option and click "Configuration".

Scroll down and click, "Google API" on the left hand settings menu. You want to enable Google Analytics and enter your website tracking code in the box provided.

![01 - Enable Google Analytics in Magento](images/01-checkout-abandonment-enable-google-analytics.png)

**Step 2:** We need to add some JavaScript to the checkout page to trak your visitors checkout process. To do this you need to make a copy of _onepage.phtml_ from the Magento base theme into your theme so we can safely make the necessary changes.

You will need to copy:  
/app/design/frontend/base/default/template/checkout/onepage.phtml

to:  
app/design/frontend/default/**<your-theme-name>**/template/checkout/onepage.phtml

Once you have copied the file to your theme you will need to add the following code to the bottom of the file:

<script type="text/javascript">// <!\[CDATA\[
   Checkout.prototype.gotoSection = function(section) {
      try {
        \_gaq.push(\['\_trackPageview', '/checkout/onepage/' + section + '/'\]);
      } catch(err) { }
      section = $('opc-'+section);
      section.addClassName('allow');
      this.accordion.openSection(section);
   };
// \]\]></script>

**Step 3:** Now that we're tracking the visitors checkout progress we now need to setup a goal within Google Analytics. To setup a goal login to Google Analytics and click on "Admin".

Scroll down and click on your website in the list to take you to the Google Analytics settings for your site.

![02 - Setup goal in google analytics](images/02-checkout-abandonment-enable-google-analytics.jpg)

In the next screen that appears click on your website name and you'll be taken to your website's profile page. Click the "Goal" tab.

![03 - Setup goal in google analytics](images/03-checkout-abandonment-setup-goal.jpg)

**Step 4:** Setup the goal funnel in Google Analytics. The goal URL is where the visitor will be sent after their payment has been processed successfully.

The default Magento URL is /checkout/onepage/success, but this may be different depending on what payment gateway your using.

![Setup a goal funnel in Google Analytics to track Magento checkout abandonment](images/04-setup-google-analytics-goal-funnel.jpg)

**Google Analytics Funnel Steps:**  
Step 1: /checkout/cart/  
Step 2: /checkout/onepage/  
Step 3: /checkout/onepage/billing/  
Step 4: /checkout/onepage/shipping/  
Step 5: /checkout/onepage/shipping\_method/  
Step 6: /checkout/onepage/payment/  
Step 7: /checkout/onepage/review/

**Step 5:** Now that we've setup our Goal Google Analytics will monitor and track the checkout process. You can test this by adding a product to the cart and going through the Magento onepage checkout process. There is normally a 24 hour delay in Google Analytics so your data won't appear instantly.

## Checkout Abandonment Funnel In Google Analytics

After a few days I logged into Google Analytics and viewed the Funnel Visualisation for my goal (Click Conversions -> Goals -> Funnel Visualization)

![Funnel visualisation of the Magento Checkout Process](images/05-google-analytics-funel-data.jpg)
