---
title: "How Can You Promote Your Business on Google+?"
date: "2013-06-12"
categories: 
  - "social-media"
tags: 
  - "business"
  - "google"
  - "social-media-2"
---

Who wants to join another social network, _right_? Don't get me wrong, I think there are too many social sites out there, but [Google Plus is different.](https://plus.google.com/)

We're not saying it is better or worse than Facebook or any other social site out there, what I am saying is that it is starting to become really popular. And whether you like it or not, it's going to become a new channel for you to promote your business.

Here is how you can leverage Google Plus to benefit your business:

### Step 1: Sign Up For a Google Account

If you have a Google Plus profile, you are already off to a great start. If you don't then go ahead and [create a Google Plus profile](https://accounts.google.com/SignUp "Create a Google Plus Profile").

### Step 2: Create Your Google+ Profile

Just because your profile is supposed to be about you, that doesn't mean you can't use it to promote your business. Here is how I would go about creating an effective Google Plus profile:

1. **Upload a picture** Ideally, one that may include you wearing a t-shirt with a company logo or something related to your business. Cyrus Shepard wrote a good article on [optimising your Google+ author picture](http://moz.com/blog/google-author-photos "Optimising your Google Author Photos"), which I recommend you read.
2. **Fill out your profile** When you're first getting started, you may feel overwhelmed with the amount of information that Google asks for. Its important that you fill in everything including links to your company, what you do in your company any relevant qualifications or education and your business contact information.
3. **Videos** If you have any videos you've been involved in or any video interviews then this is the perfect place to add that material. It will help give you more credibility and it will help your company get some extra attention.

### Step 3: Add Friends on Google+

It would seem that if you want to succeed with social media you need to connect with other people. There are two main options when it comes to adding friends. 1 - Only add people you know or 2 - Simply add anyone.

I personally prefer the second one and although you may consider it spamming, I've actually had enquiries from social media that have led to paid work from random people that I decided to add as a friend on social sites like [Facebook](http://www.quicksprout.com/2009/12/04/why-you-need-to-make-a-facebook-fan-page/) and [Twitter](http://www.quicksprout.com/2009/07/05/stop-using-twitter-for-fun/).

Ideally, what you want to do is add everyone in your companies address book as those are people you probably know through your company. You can then start adding friends and relatives to increase your social media profile even further.

Once you do that I would search for people that you feel would be interested in getting to know you or your business. If you do this for a few weeks you should easily be able to get to over 1000 people who will befriend you.

### Step 4: Join In With Conversations

When your friends post stuff on Google Plus, you should be checking out what they have to say. Granted, at this point you probably are following too many people where you won't be able to keep track of every conversation, but you should be able to keep track of a few.

If you like a conversation make sure you plus it, comment, or share it with your friends. By doing this more people on Google Plus will get to know you. And if you ever share something with people that aren't on Google Plus yet, it will email them every time you have something to share.

### Step 5: Start Conversations

Joining the conversation isn't enough. You also need to create conversations. One way I do this is by posting on my own profile so that all of my friends can see what I am thinking.

Some of the things you may want to post are status updates, quotes from famous people, things related to your business, and just random things from your daily life. If you can post something on your profile on a daily basis you should be able to get a good amount of your friends engaged on your profile.

At the same time you need to monitor which of your friends are engaging your profile so you can reciprocate and engage on theirs every once in a while.

### Step 6: Promote, Promote, Promote!

Now that you have an engaged audience, you can now promote your business. There are two main ways you can do this:

The first is to make posts on Google Plus and link to your business or website. By doing so you'll start to get more people more familiar with your business and what you do. If your business has a blog this can be very effective.

The second way to promote your business through Google Plus is by adding a [Google Plus Button](http://www.google.com/+1/button/) to your website. The more people that click the Plus Button the more traffic you'll get from Google Plus. In addition to that the more people that click on the button, the more traffic you'll get from Google's search engine.

Google has added Google+ stats in your [Webmaster Tools account](http://www.seroundtable.com/google-plus-one-webmaster-tools-13638.html) so you can see how the button is actually affecting your search engine traffic.

I hope these tips will help you get to grips with Google Plus and how you can start using social media to empower your online marketing strategy for your business.

**How do you use Google Plus for your business?** Feel free to leave your ideas and suggestions in the comments below.
