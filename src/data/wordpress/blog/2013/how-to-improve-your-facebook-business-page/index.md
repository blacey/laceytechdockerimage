---
title: "4 Steps To Improve Your Facebook Business Page"
date: "2013-01-31"
categories: 
  - "social-media"
tags: 
  - "business"
  - "facebook"
  - "social-media-2"
---

It has been one year since you launched your Facebook page and you still have a very low number of likes but most importantly none of your fans are talking about your brand - so how can you improve your **Facebook business page**?

Firstly you need to understand that (like most things) it takes time for your business to get any sort of returns on your social media investment.

Investing time to engage with potential customers over social media requires patience and long term thinking. Anything that is worthwhile takes time and focused effort to build and maintain. Here are some tips to help you improve your social media profiles:

## 1\. Ensure your page is consistent with your branding

The growth of modern business relies heavily upon Online Branding. The first place that any consumer goes to look for information on a product or service is the Internet.

Your company's online brand can be thought of as your reputation online, so its important to be consistent and professional.

You can do this by using your brand or business logo as a profile picture or using imagery that is iconic to your brand and is easily recognisable.

### Facebook Business page examples

Lockrite Security weren't on Facebook when I took on their project but I opened an account and designed their Facebook profile around their website design.

[![Lockrite Security Facebook Business Page](images/lockrite-security-facebook.jpg)](http://www.facebook.com/lockritesecurity)

I helped design the Facebook profile for DeLaBaie Skincare based around their website design.

[![DeLaBaie Skincare Facebook Business Page](images/delabaie-skincare-facebook.jpg)](http://www.facebook.com/delabaie-skincare)

## 2\. Make sure you have a good social media strategy

What is the goal of your Facebook page? Get more likes? Increase brand awareness? Increase sales? Actually, it can consist of all these goals; however, you need to be very clear.

This will help you determine the tools and tactics you need to use in order to achieve your goals. Don't create a Facebook page just because your competitors have one - you need to be able to dedicate time to keep it updated.

A key factor to a successful social media strategy is having the right resources available, so make sure you have everything you need to make things happen.

## 3\. Avoid long text status updates

Your Facebook business page should have content related to your industry. I've seen a lot of Facebook business pages that focus inward and only contain the companies latest promotions. When people come to your Facebook page, they are looking for interesting and fun content for example:

- Videos related to a product or service you sell
- Company news
- Tips/advice that's related to your industry
- Photos
- Information on local events in your industry
- Ask for customers to leave a review or Facebook recommendation
- Facebook Polls to get feedback from your customers

In other words your social media content must be relevant to your business and most importantly engaging and exciting. This does not mean that you should never post any special promotions or deals, but do something different and be sure to collect feedback on what your fans really want to see (You can do this using Facebook Polls).

## 4\. Post photos and videos

Your Facebook timeline focuses entirely on visual content so if you keep posting long status updates nobody will read them. Instead, spend some time taking high quality pictures related around your business or industry that attract attention.

People love looking at visual content (images and videos) online, and this is why websites like Vimeo, YouTube and Pintrest are so popular. There'e plenty of tools available to help you edit your photos and videos to make them look appealing.

The only reason people visit a Facebook page is because of its content. If you don't post any new content then nobody will visit your Facebook page. We live in a very fast moving world where yesterday's news is outdated content.

Be sure to post new content to your page to keep it current and up-to-date. Good content is often shared on Facebook, and every shared status. image or video means more exposure for your business.
