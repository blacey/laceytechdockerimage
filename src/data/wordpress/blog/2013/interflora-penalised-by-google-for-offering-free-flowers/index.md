---
title: "Interflora Penalised - Free Flowers For Website Backlinks"
date: "2013-03-15"
categories: 
  - "seo"
tags: 
  - "backlinks"
  - "google-penalty"
  - "seo-2"
---

Google recently penalised Interflora for their recent unethical back-linking campaign. The flower company sent a free bouquet of flowers to hundreds of bloggers in attempt to gain links back to their website.

Backlinks are an important aspect of [search engine optimisation](/services/search-engine-optimisation/ "Search Engine Optimisation (SEO)") that can improve your rank within the search engines. If done correctly your backlink profile can really help boost your website's visibility for keywords related to your business.

The flower delivery company knows that bloggers would happily write an article after receiving [cheap online flowers](http://www.cheap-flowers.co.uk/ "Cheap online flowers"), which is why they set out on this campaign.

Interflora know the importance of backlinks and how they can be used to improve your search engine rankings. Sadly this innovative marketing idea backfired and they didn't get the results they were expecting.

Instead of getting a nice SEO boost for their company for the backlinks they found they were penalised heavily by Google for unethical link-building.

## Interflora hit by Google Penguin Penalty

The Google Penguin algorithm looks at unnatural/poor quality links to a website. It focuses on link networks, paid links, blog networks and unnatural link patterns.

Interflora quickly received a lot of links from blog's, which is why Google's algorithm picked up on what they were doing and this is what led Google to penalise the website.

**Link building** on the Internet had been made harder by search engines to avoid spammy and poor quality websites from obtaining lots of links to their website to try and get to the top of the search results.

Interflora has been established for a number of years and organically they rank well for flower related searches so it seems odd that they would be wanting to potentially jeopardise their good ranking position with this targeted backlink campaign.

The problem was not that Interflora were providing bloggers with free gifts, the issue was that the links didn't use the "nofollow" attribute in their link to Interflora. **nofollow** is an attribute that can be assigned to a hyperlink to tell search engines that the link should not influence the target websites ranking in the search engine's.

A week later and it looks like Google is reducing Interflora's penalty and letting them rank in the search results. This little stunt has certainly hindered Interflora's search engine rankings and it wouldn't surprise us if it takes them a few months to regain their previous ranking positions in the search results.

What are your thoughts on Interflora link building and Google smack down situation? Leave your thoughts in the comments section below.
