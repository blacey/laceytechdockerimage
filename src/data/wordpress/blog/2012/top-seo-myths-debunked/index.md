---
title: "Top SEO Myths Debunked"
date: "2012-08-11"
categories: 
  - "seo"
tags: 
  - "seo-2"
---

Search Engine Optimisation (SEO) is an area of online marketing that is forever changing, and its something I take a great interest and experience in. This article has been written in an attempt to educate you about some of the truths and myths that exist within the SEO world, so you start to better understand some of the theory behind how websites get the top organic rankings within search engines.

A lot of changes have hit the SEO community during 2012, and it can be difficult to keep apprised of every algorithm update. Clients certainly won't know the latest Search Engine Optimisation news and its our job as website developers to help educate them and keep them up to speed with what's happening. Only then can we work together to maximise their websites potential.

## Common SEO Misconceptions

### Good Content Is All You Need

Most people have heard of the saying 'content is king' and while content is paramount to optimising your landing pages, there are other important factors that contribute to improved rankings such as on-page and off-page optimisation.

In order to apply other key aspects of off-page SEO you need good content and good on-page optimisation in the first instance, but good website optimisation isn't solely about having good content. It needs to co-exist with a well optimised website and cleverly thought out off-page back-linking strategy.

### SEO Doesn't Require Maintenance

Some businesses believe that they can optimise their site once and that Search Engine Optimisation requires no ongoing maintenance. This is a common misconception, and its why most websites fail - website optimisation is an ongoing venture that will always change.

Google and other search engines change what factors contribute to where your website ranks in the search results and if you don't adapt to these changes your website will disappear off the search results.

Even with limited budget and resources, you can review and/or expand your keyword strategy, work on link building activities and add new landings pages/content to your site.

### Website Rankings Can Be Guaranteed

For a new website or web pages, it will usually take months not weeks to start ranking for competitive keywords. Companies who guarantee results may be offering to optimise your site for low competition, long tail keywords that may result in high traffic but a low conversion rate.

No one can guarantee a #1 ranking on Google so be wary of SEO companies that claim to guarantee rankings. They may say they have a "special relationship" with Google or advertise a "priority submit" service for Google but there is no priority submit for Google or any other search engines.

### SEO Is About Manipulating Search Results

This myth is a hangover from years ago when some shady companies tried to manipulate Search Engine rankings with [Black Hat SEO](/blog/white-hat-vs-black-hat/) techniques and gave the whole industry a bad name. Today, search engine optimisation is a legitimate online marketing tool that is used by companies worldwide.

Thanks to Google's continued efforts and algorithm updates its hard for individuals or companies to manipulate the search results using black hat SEO techniques, and if you're found or suspected of using black hat SEO to manipulate the rankings you will receive a warning from Google. Failure to comply with ethical SEO standards will result in your site being de-indexed by Google.

There have been recent examples of Google banning websites that have been using techniques outside their specified guidelines. Search Engines won't send you a nice email informing you that your website has been penalised and you will see a drop in rankings.

[Google's Panda](http://googleblog.blogspot.com.au/2011/02/finding-more-high-quality-sites-in.html#!/2011/02/finding-more-high-quality-sites-in.html "Google Panda") algorithmic update shows it is sending out a clear warning that quality content and relevant link building are critical to SEO and breaches of the guidelines will not be tolerated.

### High Google PageRank equals a high ranking

PageRank (PR) is a link analysis algorithm used by Google that specifically measures link analysis. It assigns websites a numerical value based on relevance and reputation. PR is certainly an important signal in Google's algorithms but it is only one of a couple of hundred factors that determine your website's ranking on their Search Engine. However, when sourcing backlinks from websites, it is preferable to link from one with a high PR.

### Using Meta Keywords Tag Will Boost Website Rankings

Google removed the keywords tag from its ranking metrics a few years ago. Search engines got wise to spammers trying to manipulate their position in the search results using the meta keywords tags several years back and haven't used them in ranking algorithms since.

Maintaining good, relevant meta tags and descriptions will create a cleaner, more professional-looking site, it will not affect your rankings. It will, however, probably aid in getting better click-through's for existing rankings, as search engines use them for the description text on search results.

### More Links = Better Rankings

More links doesn't necessarily mean a higher rank in search engines, as it all depends on the kinds and quality of links you have on your site. The number of inbound links to your website is key in gaining higher rankings.

Your goal should be to create interesting content for your website (either pages or unique blog posts) that other people will want to link to. This will indicate to search engines that your website is relevant, and will help increase your rankings over time.

You need to be careful with link exchanges with other companies, where you link to them and they link to you (also known as a reciprocal link). Its very easy for search engines to find reciprocal links, and you might face a penalty for doing this because your effectively trying to manipulate the search engines.

Paying for links is something else you need to stay away from, yes it will increase the number of links to your site but you should do this with caution as there is a high probability they you will be buying poor quality links or worse you might have links added to websites that install viruses or sell pills and the like.
