---
title: "Can Schema.org Rich Snippets Improve Your Website SEO?"
date: "2012-12-28"
categories: 
  - "seo"
tags: 
  - "schema"
  - "seo-2"
---

## What are rich snippets?

Rich Snippets are hidden HTML tags that tell search engines what the information relates to. By giving search engines more understanding of the content on your website, you can really stand out from the competition.

All the major search engines (Google, Yahoo and Bing) use **Schema.org markup** to tell search engines what information should be considered as a Schema.org Rich Snippet in the search results.

By improving how your web page is displayed in the search results you can improve your clicks (CTR) from the search engines to your website.

You can review how many times your website was shown in search results (impressions) along with number of clicks and the click-through rate using [Google Search Console](https://search.google.com/search-console/about).

## Why Use Schema.org Rich Snippets?

The biggest benefit in using Rich Snippets is to increase your organic click-through rate (CTR). Rich snippets make your web page entry more relevant, and we all want to find the most relevant information.

Traffic to your website will often increase for two key reasons:

1. **Differentiation from the Competition** - When your listings have the added rich snippets features they will stand out more with the added photos or other features. This will increase the clicks your search listings receive and long-term will improve your search optimisation efforts by increasing rankings and overall traffic.  
      
    
2. **Help Search Engines Better Understand and Value your Content** - Improve content relevancy and weighting for search engines by providing more structured data. It will improve your local search marketing results. You will also get better quality visitors since the visitors will have more information about your page before they click on your link.

## Examples of Rich Snippets

### E-Commerce Products and Reviews

When you do a search for "[24" computer monitor](http://www.google.co.uk/search?q=24%22+computer+monitor)" on Google, you can see that product images and star reviews are shown directly on the search engine results page (SERPs). This information is being shown using **Schema Snippets**.

![01 - Rich Snippets example for computer monitor search](images/01-rich-snippets-example-1.jpg)

### Local Events

If I want to check out when a band is performing, I can search for "Bruce Springsteen Concerts" and in the listings I can see a search result listing the three upcoming dates for Bruce Springsteen.

![Rich Snippets in SERP showing concert dates in a Google Search](images/02-rich-snippets-example-2.jpg)

These are just a few examples of where **Schema.org Rich Snippets** can be used to enhance how your web pages are listed in the search results. Rich snippets can be used for a variety of different applications and each usage scenario will be shown differently in the search results examples include:

- [CreativeWork](http://schema.org/CreativeWork) - This is the most generic rich snippet example and it can include books, movies, photographs, software programs, etc.
- [Event](http://schema.org/Event) - An event happening at a certain time at a certain location.
- [Intangible](http://schema.org/Intangible) - A utility class that serves as the umbrella for a number of 'intangible' things such as quantities, structured values, etc.
- [Medical Entity](http://schema.org/MedicalEntity) - The most generic type of entity related to health and the practice of medicine.
- [Organization](http://schema.org/Organization) - An organization such as a school, NGO, corporation, club, etc.
- [Person](http://schema.org/Person) - A person (alive, dead, undead, or fictional).
- [Place](http://schema.org/Place) - Entities that have a fixed physical extension.
- [Product](http://schema.org/Product) - A product is anything that is made available for sale, for example, a pair of shoes, a concert ticket, or a car. Commodity services, like haircuts, can also be represented using this type.

## Why Do You Need Schema.org Rich Snippets?

You need to implicitly tell search engine bots what content is on our site, what it all means and what content is more valuable for our users. The rich snippets markup tells search engine bots a range of different things depending on what markup you use.

Telling search engines that you're using Rich Snippets will help make your site stand out in the search results, which will help increase the number of visitors to your site. Examples of where you can use Rich Snippets are listed below:

- Markup testimonials or product reviews using the reviews Rich Snippet
- Markup your product so Google can show product specific data in the search results (like Price, Rating etc)
- You can add Rich Snippets to your contact page around your company information
- If you use a blog you can add Rich Snippets on your post listing page, which will show the latest 3 or 4 articles in the search results under your blog page listing
- If your website advertises events you can add markup around your event listings and this will show the date, time and event name for the latest events under your page listing in Google
