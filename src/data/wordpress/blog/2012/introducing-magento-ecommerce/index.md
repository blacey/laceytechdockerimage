---
title: "Should You Use Magento For Online E-Commerce?"
date: "2012-12-10"
categories: 
  - "ecommerce-development"
tags: 
  - "magento"
---

Magento is a feature-rich E-Commerce platform built on open-source technology that provides online merchants with unprecedented flexibility and control over the look, content and functionality of their eCommerce store.

Magento's intuitive administration interface features powerful marketing, search engine optimisation and catalogue management tools to give merchants the power to create sites that are tailored to their unique business needs.

## Magento E-Commerce Features

**Analytics and Reporting:** Magento can be integrated with Google Analytics and offers many different reports

**Product Browsing:** Magento allows for multiple product images, the ability to leave product reviews, customer wishlist's and more

**Catalogue** **Browsing:** Magento provides easy navigation, advanced product filtering system and excellent product comparison

**Catalogue** **Management:** The inventory management allows you to batch import or export your products, you can specify different tax rates per location, and add additional product attributes to suit your needs

**Customer Accounts:** Magento makes it easy for customers to see their previous orders, You can setup newsletter subscriptions for email marketing

**Customer Service:** Magento comes with a contact us form, comprehensive order tracking, order history and customisable order e-mails

**Order Management:** You can create orders through admin area and your can create multiple invoices, shipments and credit memos for an order.

**Payment Gateways:** Magento offers different payment methods including: credit cards, PayPal, Authorize.net, Google Checkout, checks, money orders, support of external payment modules like Cybersource, ePay, eWAY by default but you can add to this.

**Shipping Rates:** You can ship to multiple addresses, you can setup flat rating shipping or tiered shipping, supports UPS, UPS XML (account rates), FedEx (account rates), USPS and DHL.

**Checkout:** One page checkout, SSL support, checkout without having an account.

**Search Engine Optimisation:** Has limited SEO support out the box unless you pay for Magento SEO Extensions to enhance the capabilities

**International Support:** Multiple languages and currencies, list of allowed countries for registration, purchasing and shipping and you have a number of localisation options.

**Marketing Promotions:** You can create coupons / discounts with different promotion options.

### Magento Minimum Requirements

The system requirements for Magento are considerably higher than most websites and because of this Magento cannot be used on shared web hosting packages.

<table width="100%"><tbody><tr><th>Supported Operating Systems</th><td>Linux x86, x86-64</td></tr><tr><th>Supported Web Servers</th><td>Apache 1.3.x Apache 2.0.x Apache 2.2.x Nginx (Magento 1.7 Community and 1.12 Enterprise versions)</td></tr><tr><th>Supported Browsers</th><td>Microsoft Internet Explorer 7 and above Mozilla Firefox 3.5 and above Apple Safari 5 and above on Mac only Google Chrome 7 and above Adobe Flash browser plug-in should be installed</td></tr><tr><th>PHP Compatibility</th><td>5.2.13 - 5.3.15</td></tr><tr><th>Required PHP Extensions</th><td>PDO_MySQL simplexml mcrypt hash GD DOM iconv curl SOAP (if Webservices API is to be used)</td></tr><tr><th>MySQL Compatibility</th><td>4.1.20 or newer InnoDB storage engine</td></tr><tr><th>Memory (Ram) Needed</th><td>No less than 256mb (Preferably 512mb or more)</td></tr><tr><th>Server hosting setup</th><td>Ability to run scheduled jobs (crontab) with PHP 5 Ability to override options in .htaccess files</td></tr></tbody></table>

**Please Note:** - The system requirements above should only be used as reference. They are correct as of March 2013 for Magento 1.7 Community Edition.
