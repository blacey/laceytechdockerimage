---
title: "Why Are CSS Print Stylesheets Important For Your Website?"
date: "2012-07-15"
categories: 
  - "development"
tags: 
  - "css"
  - "print"
---

When developing a website, [most developers think about what they see on screen](https://http://laceytech.local/pwa/blog/cheap-website-development-isnt-always-good/) and in code, and more often than not they forget that people will want to print out articles or useful information from a website.

How many times have you clicked your print icon in your web browser to find that 10 or 20 pages have been printed for a single page of information? This is a problem that will continue to frustrate web users and its easily resolved using **print stylesheets**.

I appreciate that a lot of web users use tablets, iPads and other hand-held devices to browse the web but we don't live in a paperless world and users will still print out pages and articles to read.

Reading something on paper is less tiring on the eyes than reading something on screen. I personally find myself printing off guitar chords online, and most websites don't thing that their content will be printed, which makes it a usability nightmare.

## What is a print stylesheet?

Print stylesheets are used to change how a page looks when the user prints a web page. Typically you would remove anything that isn't related to the page content being viewed (website navigation, sidebars, page footers etc). This means that only the content of the page is printed.

## How do I make a print stylesheet?

You would need to open your text editor or IDE (Integrated development environment) and create a file in your public css folder called print.css. Once you've done that you need to add the following line of code to the <head> of your website. Be sure to change the href attribute so it points to the location of your print.css file.

<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />

The `media="print"` attribute ensures that the styles contained in this file are only used when printing the document. You need to make sure that your main CSS stylesheet has the `media="screen"` attribute otherwise you will run into problems.

## What do I put in my print stylesheet?

As a general rule its best to hide elements within your website that is interactive or otherwise irrelevant to the page or article content.

### Enlarge your content area

The first thing you would want to do is make sure the printed document uses the whole width of the page, you can do this by adding the following code to your CSS document:

body, #content {
width: 100%;
margin: 0;
float: none;
}

### Remove site navigation

Websites are interactive and often contain menus to other areas of the site. When a document is printed there is no need to show the websites navigation menus as these are useless on paper.

nav, #nav{
display: none;
}

### Hide screen specific content areas

Its personal preference if you decide to hide sidebar content on your website, as some sites store related content to the left or right hand side of the article. Most of the content in my sidebars are not directly related to the content so I hide them.

#left-sidebar, #right-sidebar, #footer{
display: none;
}

Another approach is to make sure your sidebar content areas have a unique identifier (an ID or a class), which would allow you to hide specific content when its printed.

Its also advised that you change the css of your sidebar to `display: block;` so sidebar content appears after the article when its printed.

The reason we change the css to display:block is to remove the float: left or float: right attributes fro the sidebar(s).

#right-sidebar { display: block; }
#right-sidebar .related-posts { display: none; }
#right-sidebar .free-quote { display: none; }

### Restyling typography for print media

Web typography is handled differently to that of printed media. Websites can use font sizes such as em, px (pixels), pt (point), and percentages.

When it comes to print you're better off using 12pt (point) as this is the standard font size. I personally keep the bottom margin of h1 tags the default, but I enforce a smaller gap for all other headings to keep the page typography nice and clean.

h1 { font-size: 18pt; }
h2 { font-size: 16pt; }
h3 { font-size: 14pt; }
h2, h3, h4 { margin-bottom: 10pt }
p { font-size: 12pt; }

### Using the right font family to maximize readability

In print design most people prefer serif fonts because they are easier to read. Setting the font-family to serif in your print stylesheet is probably a good idea, although some readers may be surprised to find that the font in their print-out is not the same as the one on your website.

body { font-family: Georgia, "Times New Roman", serif; }

**Warning:** Don't remove information that could be relevant to the user if they are printing the page. Only **remove non-essential web elements when printing** (Navigation, sidebars, footer links etc) the website.

If your website uses background colours it is advised that you remove them, and if you use a white logo on dark background on-screen then its a good idea to use a grey-scale logo when the page is printed.
