---
title: "Contributing To The SEOMoz Community"
date: "2012-04-20"
categories: 
  - "seo"
tags: 
  - "seo-2"
  - "seo-community"
  - "seomoz"
---

SEOMoz is a large community of **search engine optimisation** specialists who discuss the latest news and updates from the SEO world.

Our managing director (Ben Lacey) has been a member of Moz for 2 months now and I've helped a number of people within the community, and I've been lucky enough to receive three staff endorsements for answers I've provided.

For your contributions and time spent researching and helping others you are rewarded points (MozPoints), which entitle you to some fun benefits from the SEOMoz community, when you reach 199 MozPoints you get a hug from Roger, the SEOMoz mascot).

Today marks a milestone in my contributions to the search engine community, as our managing director received the rank of Journeyman on the SEOMoz website for earning 200 MozPoints.

Our MD answered a number of questions within the community ranging from technical questions, general search engine optimisation advice and how to optimise WordPress to increase your rank within search engines.

**Some questions I've been involved in:**

- [Can a Joomla template ruin a sites on-page seo?](http://www.seomoz.org/q/can-a-joomla-template-ruin-a-sites-on-page-seo "Can a Joomla template ruin a sites on-page seo?")
- [Need help setting up Google Analytics goal tracking](http://www.seomoz.org/q/need-help-setting-up-google-analytics-goals-tracking "Need help setting up Google Analytics goal tracking") **Received Staff Endorsement**
- [Resolve WordPress duplicate content](http://www.seomoz.org/q/wordpress-duplicate-content "Resolve WordPress duplicate content")
- [WordPress blog domain mapping help](http://www.seomoz.org/q/wordpress-blog-hosted-on-godaddy-domain-mapping-help "WordPress blog domain mapping help") **Received Staff Endorsement**
