---
title: "Building A Career With Lacey Tech Solutions (Q&A)"
date: "2019-04-04"
categories: 
  - "news"
---

Here at Lacey Tech Solutions, we believe that everyone has to start somewhere. With this in mind, in the last few months we have been running a work experience scheme with young apprentices.

Dom came to us in search of that all important, **worthwhile experience** and the chance to get started on the path of a career into the industry.

Work experience is essential for getting a job in the modern world. Whether it's a short work placement or a longer internship, work experience is always viewed favorably by employers and can help you decide your future career.

Dom has been working on a variety of projects such as:

- Content Writing
- [Building a website](/services/website-development/) in PHP
- Learning Object-Orientated PHP
- Co-Developing a web application
- Social media work
- Using APIs to share information between online services

Since starting his work experience with Lacey Tech Solutions, Dom has come on leaps and bounds and is never afraid to adapt and learn new skills. Dom has learnt a lot of key skills that will adequately prepare him for the website development industry.

Seeing Dom's initial progress on the work experience placement, we wanted to offer Dom a part-time role. His can-do attitude is perfect for him to experience working within our broad range of services.

Our managing director (Ben Lacey) has been tutoring Dom with the website development aspects of his role. Having been in the website development industry for over 12 years, Ben has had a lot of experience and unique insights to share with Dom.

We've been really pleased with Dom's work and we look forward to helping him at the start of his promising career as a website developer.

Lacey Tech Solutions like to give something back to the community and the work experience scheme has not only helped Dom, but the company over all.

## Work Experience Q&A

Below is a Questions & Answers section where you can learn more about Dom and his role within Lacey Tech Solutions.

### What attracted you to gain work experience with Lacey Tech Solutions?

I've always been attracted to computers, and coding, and the need for a work experience placement with my school was the perfect opportunity to learn more about the technology industry for real situations and the future. Lacey Tech Solutions was the company that stood out to me as, as well as creating great websites for their customers, their website was just as good-looking and I think it's been perfect in teaching me the ways the industry works in real situations.

### What have you learned during your time with the company?

During my time with the company I have learned many things including the ways that things are expected to be done overall in the industry and I have also learnt a lot more about the coding and promotional aspects of the job. The experience helped me improve my original knowledge and it introduced me to a lot of the best practice elements of the job.

### What have you enjoyed most about working with Lacey Tech Solutions?

I have enjoyed many things about working with Lacey Tech Solutions but the thing I enjoyed the most was the amount and depth of information Ben imparted on me.

### How did it feel working remotely from home?

Working remotely from home was ideal for me as it allowed me to work well and in my own environment without being constrained by the times, I would've been able to get to and from a workplace.

### Was there anything you didn't enjoy during your work experience?

There wasn't anything I didn't enjoy about my work experience – it was a lot of fun to gain the experience of something I have a real interest in, in a real situation.

### What does a typical day involve?

A typical day involves many things, revolving mainly on the instructions communicated from Ben. Having tasks varying and changing from day-to-day helps me stay interested in the jobs at hand.

### How did you feel when Ben offered you a role within the company?

Being offered a job within the company was very exciting and definitely boosted my confidence in my own ability and it has helped me to take a hobby to a job.

### What impact has Ben had on your learning and development experience?

Ben has taught me many things which was very beneficial to my development experience.

### Was there anything you found challenging in your role?

What was challenging, for me, was trying to produce the expected things, but I'm always up for a challenge and the challenges in the role have helped me improve greatly.

### **How could Lacey Tech Solutions improve for future work experience placements?**

Personally, I wouldn't have changed anything about the work experience as it was perfect for me.

### **What advice would you give to others looking to gain work experience or a career within Lacey Tech Solutions?**

Hard work becomes a lot easier when you're doing something you love and I suggest that you should make sure you are passionate about what you want to do in order to produce both the best results for yourself and for the company.

### **What do you like doing in your spare time?**

When I'm not dancing, I like to listen to music, do photography and code.

### **What is your favourite food?**

My favourite food is pasta, but, to be honest, anything edible is good for me.

### **What is your favourite singer/band and song?**

My favourite singer is Taylor Swift and my favourite song is 'New Year's Day'.

### **What is your favourite TV Programme and Film?**

My favourite TV programme is Riverdale and my favourite film is 'The Conjuring'.

### **How would you best describe yourself?**

Shy, Techie, Musical and Smart.

### **What is your greatest achievement so far?**

Designing a website from scratch.

### **Do you have a favourite quote?**

"I'm not a princess, this ain't a fairytale"

### **What is your favourite book?**

'Of Mice and Men

### **Testimonial**

> The experiences I've had with Lacey Tech Solutions are ones I will remember – it's great to learn from [experienced industry professionals](/about/).
> 
> Dom

## Work Experience Placements

If you would like to find out more about our services, our processes or who we work with at Lacey Tech Solutions, we're happy to give you an insight into how we work and what goes on behind the scenes. Contact us today on **01252 518 233** to discuss [work experience opportunities](/work-experience/).
