---
title: "The Link Tax and Meme Ban Copyright Directive"
date: "2019-04-08"
categories: 
  - "news"
---

In September, this year, many a content creator was watching with interest as the European Parliament deliberated on and voted in favour of the European Directive on Copyright in the Digital Free Market.

The document, primarily, ensures that both content producers and content sharing platforms, including video hosting sites like YouTube and social media platforms like Twitter or Facebook, must do more **to stop copyrighted material being used and shared** by those who don’t hold the rights to them.

Out of all the Directive on Copyright, two articles have grabbed the attention of content creators, in particular, Article 11 and Article 13. Here, we’re going to explore these new copyright law articles in a little more detail.

## Article 13 is bad for content creators

Article 13 could shut down millions of channels on YouTube in the European Union (EU), which could affect other content distribution websites. This directive may be finalised by the end of 2018 but no formal date has been set. The aim of the directive is to protect copyright holders on the Internet.

## The Directive on Copyright, the Meme Ban, Article 11 and Article 13

The European Union Directive on Copyright in the Digital Free Market is often referred to as “Article 13”, after the most contentious article contained within it. Some have taken to referring to it as the “Meme Ban”, based on a common interpretation of Article 13.

In truth, Article 13 is just one of fifteen articles contained in the Directive on Copyright, and Article 11 is another. However, it’s these two articles that have gained the most attention and are the most substantial.

> "The Directive on Copyright would make online platforms and aggregator sites liable for copyright infringements, and supposedly direct more revenue from tech giants towards artists and journalists. Under current legislation, platforms such as YouTube aren’t responsible for copyright violations, although they must remove that content when directed to do so by the rights holders."
> 
> **Source:** [Wired.com](https://www.wired.co.uk/article/what-is-article-13-article-11-european-directive-on-copyright-explained-meme-ban)

As with all stories, there is another side to the Directive of Copyright. Those in favour of it have argued that copyrighted material is distributed too freely on the internet. It’s not difficult for content uploaders to share pirated music and even full movies on Youtube, for instance.

As a result, many an online user are enjoying the copyrighted material, whether reading, watching, or listening to it, without any compensation for the original creators and copyright holders. Others argue that the Directive on Copyright goes much too far.

### Why content creators are worried about Article 11 and Article 13

Struggles with copyright law are nothing new to the world of online content creation and sharing, but the relationship between content creators, online platforms, and copyright holders may be about to get a lot more contentious.

Already, YouTube has been plagued with issues of content creators having their videos flagged, demonetised, or removed entirely at the request of copyright holders, even for videos that make legitimate use of Fair Use laws.

Articles 11 and 13 effectively shift the responsibility away from copyright holders to these online platforms, instead. The onus is on social media websites, YouTube, **and content creators who publish content on their own website**, to make sure none of the material can be defined as the illegal use of a copyright.

Many content creators believe that the platforms mentioned are already doing a terrible job of ensuring that “copyright strikes” are used correctly. With the potential of having to pay a fee for every infraction, many believe that these online content sharing platforms will get conservative and over-reactive to even the slightest hint of copyright misuse.

As a result, some fear that sites like YouTube and Twitter will immediately remove any content that uses copyrighted material of any kind, even if it should be rightfully protected under Fair Use.

## Article 13 - The Meme Ban

Memes can be an image, video or piece of text that are often shared on Social Media and are mainly humorous in nature, but some are created to shock you or portray something controversial. The word, "Meme" originates from the Greek word "Mimema" that loosely translates to, "Something Imitated".

- ![](images/Webp.net-resizeimage.jpg)
    
- ![](images/Webp.net-resizeimage-1.jpg)
    

By far the most contentious part of the Directive on Copyright, Article 13 is often conflated with the whole Directive, or referred to as the “Meme Ban”. On the face of it, Article 13 may look like it’s asking content sharing platforms (like YouTube, Soundcloud, Tumblr, etc.) to take down copyrighted material, as always.

However, no longer is that infringing material to be found by copyright holders, who then submit a petition to remove it. Instead, the responsibility is on the platform and there is no indication in the Directive of how, exactly, sites and online platforms are supposed to identify and remove this content.

In the past, Article 13 contained references to “proportionate content recognition technologies”, which many read as automated platforms that would scan any and all content for copyright infringement, which some argue would erase Fair Use.

That clause is gone now, replaced by text on how “special account shall be taken of fundamental rights, the use of exceptions and limitations as well as ensuring that the burden on SMEs remains appropriate and that automated blocking of content is avoided.”

The title of “the Meme Ban”, came after some argued that content creators can no longer identify what will be a copyright infringement and what is at risk of being taken down. Will memes, often based on copyrighted material, be removed?

<iframe src="https://www.youtube-nocookie.com/embed/GqjNvtX_Xbw" width="100%" height="400px" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Will critical videos make use of copyrighted future under Fair Use be removed? Even if the Directive on Copyright doesn’t count them as copyright infringements, the fear is that memes and other parody or Fair Use content will be removed simply because the automated filters, or whatever other tools content platforms might use, won’t be able to make the distinction and will remove them anyway.

## Article 11 - The Link Tax

Slightly lesser known, but also gaining some heat is Article 11, which has been referred to by some as the “link tax”. This is the article that states that any news or content aggregator sites must pay the publishers of any content they share on their website. 

While proponents argue that this is simply ensuring that news articles don’t get copied from one site to another wholesale, the devil is once again in the details of how this would work in practice.

> "Some extreme interpretations have suggested that this might even stop ordinary web users sharing new stories, but the text of Article 11 does exempt individuals. It says that the new rights given to publishers “shall not prevent legitimate private and non-commercial use of press publications by individual users."
> 
> **Source:** [TheVerge.com](https://www.theverge.com/2018/9/13/17854158/eu-copyright-directive-article-13-11-internet-censorship-google)

It’s unclear, for instance, how much of an article a site would have to share before paying. Site owners don’t know, so there are questions of whether there would be fees for quoting articles, as the only exception listed by Article 11 is “mere hyperlinks which are accompanied by individual words,” which is rarely how links and articles are shared. Social platforms are excluded, but even that is up for debate.

## The Future of Article 11 and Article 13

**The Directive on Copyright has officially been passed by the European Parliament**. Now, it’s involved in ongoing discussions by the European Council, Parliament, and Commission.

The final version is expected to be passed back to the European Parliament early next year. If it passes, all EU nations will have to pass it within two years and, as struggles with the GDPR and E-Privacy have shown, this can have wider implications on websites and website users across the world.

### Resources and Further Reading

- [A Hitchhikers Guide to the Link Tax — delivering your message to MEPs](https://openmedia.org/en/hitchhikers-guide-link-tax-delivering-your-message-meps)
- [New study shows Spain’s “Google tax” has been a disaster for publishers](https://arstechnica.com/tech-policy/2015/07/new-study-shows-spains-google-tax-has-been-a-disaster-for-publishers/)
- [The EU has passed Article 13, but Europe's meme war is far from over](https://www.wired.co.uk/article/eu-article-13-passed-meme-war)
- [Memes 'will be banned' under new EU copyright law, warn campaigners](https://news.sky.com/story/memes-will-be-banned-under-new-eu-copyright-law-warn-campaigners-11398577)
- [What is Article 13? EU copyright rules EXPLAINED](https://www.express.co.uk/news/world/1022148/Article-13-EU-copyright-law-explained-eu-meme-ban)
- [EU smacks internet in the face with link tax and upload filter laws](https://www.zdnet.com/article/eu-smacks-internet-in-the-face-with-link-tax-and-upload-filter-laws/)
