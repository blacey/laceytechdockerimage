import React from 'react'
import Hero from '../../../components/elements/Hero'
import PageContent from '../../../components/elements/PageContent'

export default function BlogSingle() {
    return (
        <PageContent>
            <Hero heroName="blogSingle">
                <h1>Single Category Page</h1>
            </Hero>    
        </PageContent>
    )
}