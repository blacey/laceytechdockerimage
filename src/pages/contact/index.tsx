import React from 'react'
import Hero from '../../components/elements/Hero'
import PageContent from '../../components/elements/PageContent'
import ContactForm from '../../forms/ContactForm'

function index() {
    return (
        <>
            <Hero heroName="contact">
                <h1>Contact Us</h1>
            </Hero>

            <PageContent>
                <section style={{display:'none'}}></section>
                <section className="contact-intro">
                    <div className="container">
                        <div className="row">
                            <p>Please use the form below to contact us about our products, services and customer service queries. You can call our office on <strong>01252 518233</strong> if you would prefer to discuss your requirements.</p>
                        </div>
                        
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <div style={{display:'flex', justifyContent:'flex-start', alignItems: 'center', height: '400px', width: '100%'}}>
                                    <ContactForm />
                                </div>
                            </div>

                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <div style={{display:'flex', justifyContent:'center', alignItems: 'center', height: '400px', width: '100%', background: '#EEE'}}>
                                    Map Goes Here
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </PageContent>
        </>
    )
}

export default index
