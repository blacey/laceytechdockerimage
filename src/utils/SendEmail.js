const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const email = {
    to: 'ben.lacey57@gmail.com',
    from: 'ben@laceytechsolutions.co.uk',
    subject: 'Sending with SendGrid is Fun',
    text: 'and easy to do anywhere, even with Node.js',
    html: '<strong>and easy to do anywhere, even with Node.js</strong>',
}

sgMail.send(email)
.then(() => {
    console.log('Email has Been Sent')
})
.catch((error) => {
    console.error("Email Not Sent - " & error)
})