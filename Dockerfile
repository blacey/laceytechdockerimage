FROM node:14

RUN mkdir -p /usr/src/laceytechwebsite/

WORKDIR /usr/src/laceytechwebsite/

COPY ./package.json /usr/src/laceytechwebsite/

RUN npm install

COPY . /usr/src/laceytechwebsite/

#RUN npm run build 

EXPOSE 3000

CMD [ "npm", "run" , "dev" ]